Aplicación de gestión de licencias
==================================

Esta aplicación facilita la tramitación de licencias de obras impresas o susceptibles de serlo. Se integra con el [módulo de gestión de derechos de Moodle](https://bitbucket.org/ujiapps/moodle-local_lpi).

Configuración
-------------

Es necesario crear un archivo de configuración cuyo contenido pasamos a describir:


```
# Database
# ========
# La aplicación ha sido probada principalmente con Oracle

uji.db.username=<usuario de la base de datos>
uji.db.password=<contraseña>
uji.db.driverClass=<por ejemplo oracle.jdbc.OracleDriver>
uji.db.jdbcUrl=<connection string de la Base de datos>
uji.db.preferredTestQuery=<por ejemplo select 1 from dual;>
uji.db.databaseId=<Id de la Base de datos, por ejemplo: ORACLE>
uji.db.dialect=<por ejemplo org.hibernate.dialect.OracleDialect>

# Autenticacion
# ===========
# La webapp está integrada con el SSO UJI, pero se permite el acceso local 
# sin autenticación cuando accedemos mediante localhost. Para que esta 
# configuración se active correctamente son necesarias las siguientes propiedades.

uji.deploy.defaultUserName=9792
uji.deploy.defaultUserId=borillo
uji.deploy.returnScheme=http
uji.deploy.returnHost=localhost
uji.deploy.returnPort=8080
uji.deploy.authToken=xxxxxxx

# Integración con Moodle
# ==================
# URL y token de conexión a Moodle para poder consultar las peticiones de gestión
# de derechos pendientes de tramitar.

uji.external.authToken=<token del usuario LPI Integration User, tal cual está descrito en la guía de administración del módulo GDPI>
uji.external.endPoint=<[wwwrootmoodle]/webservice/rest/server.php>
```

Instalación
-----------

El código del plugin se gestiona con Git y está disponible en Bitbucket. 
Para la instalación del código puede seguir los siguientes pasos:

1.- Descargar el código utilizando Git:
 
```
$ git clone https://bitbucket.org/ujiapps/uji-mdl-base.git
```

2.- Compilar el código con maven para generar un archivo .war


```
$ cd uji-mdl-base/
$ mvn -DskipTests clean package
```
 
3.- Instalar el archivo .war en un servidor de aplicaciones. En nuestro caso se ha utilizado tomcat 8, y los pasos a seguir para desplegar la aplicación son los siguientes:


```
$ cd target/
$ cp web.xml [tomcat_path]/webapps
```
 
4.- La aplicación estará disponible a través de un navegador, en la dirección http://localhost:8080/mdl

Licencia
--------

Este proyecto se distribuye bajo licencia dual GPLv3 y EUPLv1.2. Puede encontrar información adicional [aquí](https://www.uji.es/ujiapps/llicencia).
