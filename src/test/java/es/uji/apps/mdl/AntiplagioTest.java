package es.uji.apps.mdl;

import es.uji.apps.mdl.base.dao.AntiplagioDAO;
import es.uji.apps.mdl.base.model.antiplagio.IdDocumento;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
public class AntiplagioTest {

    @Autowired
    AntiplagioDAO antiplagioDAO;

    @Test
    public void testObtenerPeticionesPendientes () {

        List<IdDocumento> idsPeticiones = this.antiplagioDAO.obtenerPeticionesPendientesDeChequeo();
        Assert.assertTrue(true);
    }
}
