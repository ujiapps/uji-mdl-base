package es.uji.apps.mdl;

import com.fasterxml.jackson.databind.JsonNode;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.ws.rs.core.MediaType;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.nullValue;
import static org.hamcrest.number.OrderingComparison.greaterThanOrEqualTo;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
public class ExternalApiTest
{
    @Value("${uji.external.endPoint}")
    private String endPoint;

    @Value("${uji.external.authToken}")
    private String token;

    @Test
    public void service()
    {
        WebResource getPeticiones = Client.create()
                .resource(endPoint)
                .queryParam("wsfunction", "local_lpi_get_pending_files")
                .queryParam("moodlewsrestformat", "json")
                .queryParam("wstoken", token)
                .queryParam("page", "0")
                .queryParam("count", "25")
                .queryParam("search", "");

        ClientResponse response = getPeticiones.type(MediaType.APPLICATION_JSON).post(ClientResponse.class);

        assertThat(response.getStatus(), is(200));

        JsonNode root = response.getEntity(JsonNode.class);

        assertThat(root.get("errorcode"), nullValue());
        assertThat(root.get("count").asInt(), greaterThanOrEqualTo(0));
    }
}
