--
-- Inicializa los datos mínimos para el servicio de borrado de cursos.
--

SET DEFINE OFF;
Insert into GRA_POD.POD_MOODLE_BORRADO_ESTADOS
(NOMBRE, DESCRIPCION)
Values
    ('ERROR', 'Se transita ante cualquier error.');
Insert into GRA_POD.POD_MOODLE_BORRADO_ESTADOS
(NOMBRE, DESCRIPCION)
Values
    ('PENDIENTE', 'Es el estado incial. No se ha hecho ninguna acción.');
Insert into GRA_POD.POD_MOODLE_BORRADO_ESTADOS
(NOMBRE, DESCRIPCION)
Values
    ('BACKUP_INICIADO', 'El LMS ha iniciado el backup del curso.');
Insert into GRA_POD.POD_MOODLE_BORRADO_ESTADOS
(NOMBRE, DESCRIPCION)
Values
    ('BACKUP_FINALIZADO', 'El LMS ha terminado con éxito el backup del curso.');
Insert into GRA_POD.POD_MOODLE_BORRADO_ESTADOS
(NOMBRE, DESCRIPCION)
Values
    ('BORRADO_EN_ORACLE', 'El curso se ha borrado en Oracle.');
Insert into GRA_POD.POD_MOODLE_BORRADO_ESTADOS
(NOMBRE, DESCRIPCION)
Values
    ('BORRADO_EN_LMS_INICIADO', 'El borrado del curso se ha iniciado en el LMS');
Insert into GRA_POD.POD_MOODLE_BORRADO_ESTADOS
(NOMBRE, DESCRIPCION)
Values
    ('BORRADO_EN_LMS_FINALIZADO', 'El borrado del curso ha terminado con éxito en el LMS');
Insert into GRA_POD.POD_MOODLE_BORRADO_ESTADOS
(NOMBRE, DESCRIPCION)
Values
    ('FINALIZADO', 'El borrado de curso ha terminado con éxito.');
COMMIT;
