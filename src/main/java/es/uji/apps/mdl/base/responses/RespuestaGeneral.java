package es.uji.apps.mdl.base.responses;

import java.util.ArrayList;
import java.util.List;

import es.uji.apps.mdl.base.model.Encuesta;

public class RespuestaGeneral<T>
{
    private Boolean success;
    private Long count;
    private List<T> data;

    public RespuestaGeneral(Boolean success, List<T> data)
    {
        this.success = success;
        this.count = (long) data.size();
        this.data = data;
    }

    public RespuestaGeneral(Boolean success, Long count, List<T> data)
    {
        this.success = success;
        this.count = count;
        this.data = data;
    }

    public static RespuestaGeneral<Encuesta> toRespuesta(Encuesta e)
    {
        List<Encuesta> data = new ArrayList<>(1);
        data.add(e);
        return new RespuestaGeneral<>(true, 1l, data);
    }

    public Boolean getSuccess()
    {
        return success;
    }

    public Long getCount()
    {
        return count;
    }

    public List<?> getData()
    {
        return data;
    }
}
