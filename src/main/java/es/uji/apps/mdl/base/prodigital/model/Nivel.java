package es.uji.apps.mdl.base.prodigital.model;

public enum Nivel {
    A1,
    A2,
    B1,
    B2,
    C1,
    C2;
}
