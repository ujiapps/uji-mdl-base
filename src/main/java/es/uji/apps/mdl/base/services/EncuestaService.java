package es.uji.apps.mdl.base.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.mdl.base.dao.EncuestaDAO;
import es.uji.apps.mdl.base.model.Encuesta;

@Service
public class EncuestaService
{
    @Autowired
    EncuestaDAO encuestaDAO;

    /**
     * Devuelve las base de evaluación de profesorado de máster y/o grado pendientes de contestar.
     *
     * @param perId
     * @return
     */
    public Encuesta getEncuentasPendientes(Long perId)
    {
        return this.encuestaDAO.getEncuestasPendientes(perId);
    }
}
