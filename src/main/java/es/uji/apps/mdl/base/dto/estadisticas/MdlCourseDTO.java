package es.uji.apps.mdl.base.dto.estadisticas;

import javax.persistence.Column;

public class MdlCourseDTO {

    private String fullname;
    private String shortname;
    private String procesar;
    private Long oraCourseId;
    private String visible;


    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    public String getProcesar() {
        return procesar;
    }

    public void setProcesar(String procesar) {
        this.procesar = procesar;
    }

    public Long getOraCourseId() {
        return oraCourseId;
    }

    public void setOraCourseId(Long oraCourseId) {
        this.oraCourseId = oraCourseId;
    }

    public String getVisible() {
        return visible;
    }

    public void setVisible(String visible) {
        this.visible = visible;
    }
}
