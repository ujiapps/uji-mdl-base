package es.uji.apps.mdl.base.services;

import es.uji.apps.mdl.base.dto.enrol.CreatedCourseDTO;
import es.uji.apps.mdl.base.model.enrol.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.mdl.base.dao.EnrolDAO;

@Service
public class EnrolService
{
    @Autowired
    EnrolDAO enrolDao;

    /**
     * Marca un curso como creado.
     *
     * @param courseid el id de curso.
     */
    public void markCourseAsCreated(Long courseid)
    {
        this.enrolDao.markCourseAsCreated(courseid);
    }

    /**
     * Marca un curso como creado.
     *
     * @param courseid el id de curso.
     */
    public void markCourseAsCreated(CreatedCourseDTO createdCourseDTO)
    {
        this.enrolDao.markCourseAsCreated(createdCourseDTO);
    }


}
