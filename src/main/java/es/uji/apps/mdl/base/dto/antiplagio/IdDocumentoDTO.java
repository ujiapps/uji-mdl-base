package es.uji.apps.mdl.base.dto.antiplagio;

import es.uji.apps.mdl.base.model.antiplagio.IdDocumento;

public class IdDocumentoDTO
{
    private Long id;
    private String idExterno;

    public IdDocumentoDTO(IdDocumento idDocumento)
    {
        this.id = idDocumento.getId();
        this.idExterno = idDocumento.getIdExterno();
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getIdExterno()
    {
        return idExterno;
    }

    public void setIdExterno(String idExterno)
    {
        this.idExterno = idExterno;
    }
}
