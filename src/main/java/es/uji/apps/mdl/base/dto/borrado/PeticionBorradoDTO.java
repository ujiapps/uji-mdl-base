package es.uji.apps.mdl.base.dto.borrado;

import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.mdl.base.model.borrado.PeticionBorrado;

public class PeticionBorradoDTO
{
    private Long id;
    private Long pmeId;
    private Date fechaSolicitud;
    private String estado;
    private Long solicitanteId;
    private String info;

    public static PeticionBorradoDTO toDTO (PeticionBorrado peticionBorrado)
    {
        PeticionBorradoDTO peticionBorradoDTO = new PeticionBorradoDTO();

        peticionBorradoDTO.setId(peticionBorrado.getId());
        peticionBorradoDTO.setPmeId(peticionBorrado.getPmeId());
        peticionBorradoDTO.setFechaSolicitud(Date.from(peticionBorrado.getFechaSolicitud().atZone(ZoneId.systemDefault()).toInstant()));
        peticionBorradoDTO.setEstado(peticionBorrado.getEstado());
        peticionBorradoDTO.setSolicitanteId(peticionBorrado.getSolicitanteId());
        peticionBorradoDTO.setInfo(peticionBorrado.getInfo());

        return peticionBorradoDTO;
    }

    public static List<PeticionBorradoDTO> toDTO (List<PeticionBorrado> peticionesBorrado)
    {
        return peticionesBorrado.stream().map(PeticionBorradoDTO::toDTO).collect(Collectors.toList());
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getPmeId()
    {
        return pmeId;
    }

    public void setPmeId(Long pmeId)
    {
        this.pmeId = pmeId;
    }

    public Date getFechaSolicitud()
    {
        return fechaSolicitud;
    }

    public void setFechaSolicitud(Date fechaSolicitud)
    {
        this.fechaSolicitud = fechaSolicitud;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public Long getSolicitanteId()
    {
        return solicitanteId;
    }

    public void setSolicitanteId(Long solicitanteId)
    {
        this.solicitanteId = solicitanteId;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
