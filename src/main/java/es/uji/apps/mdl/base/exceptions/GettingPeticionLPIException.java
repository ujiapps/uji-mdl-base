package es.uji.apps.mdl.base.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

public class GettingPeticionLPIException extends CoreDataBaseException
{
    public GettingPeticionLPIException()
    {
        super("Errada recuperant una petició");
    }

    public GettingPeticionLPIException(String message)
    {
        super(message);
    }
}
