package es.uji.apps.mdl.base.dao;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.mysema.query.Tuple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;

import es.uji.apps.mdl.base.model.antiplagio.IdDocumento;
import es.uji.apps.mdl.base.model.antiplagio.PeticionChequeo;
import es.uji.apps.mdl.base.model.antiplagio.PeticionChequeoExtendida;
import es.uji.apps.mdl.base.model.antiplagio.ProfesorTFG;
import es.uji.apps.mdl.base.model.antiplagio.QPeticionChequeo;
import es.uji.apps.mdl.base.model.antiplagio.QPeticionChequeoExtendida;
import es.uji.apps.mdl.base.model.antiplagio.QProfesorTFG;
import es.uji.commons.db.BaseDAODatabaseImpl;

import javax.persistence.TypedQuery;

@Repository
public class AntiplagioDAO extends BaseDAODatabaseImpl
{
    private QPeticionChequeo qPeticionChequeo = QPeticionChequeo.peticionChequeo;
    private QProfesorTFG qProfesorTFG = QProfesorTFG.profesorTFG;
    private QPeticionChequeoExtendida qPeticionChequeoExtendida = QPeticionChequeoExtendida.peticionChequeoExtendida;

    public List<IdDocumento> obtenerPeticionesPendientesDeChequeo()
    {
        JPAQuery query = new JPAQuery(this.entityManager);
        return query
                .from(qPeticionChequeoExtendida)
                .where(qPeticionChequeoExtendida.target.toUpperCase().in("TFG", "TFM"),
                        qPeticionChequeoExtendida.estado.eq("PENDIENTE"))
                .list(qPeticionChequeoExtendida.id, qPeticionChequeoExtendida.idExterno, qPeticionChequeoExtendida.asignaturaId, qPeticionChequeoExtendida.cursoAcademico)
                .stream()
                .map(IdDocumento::new)
                .collect(Collectors.toList());
    }

    public List<IdDocumento> obtenerPeticionesEnviadas()
    {
        JPAQuery query = new JPAQuery(this.entityManager);
        return query.from(qPeticionChequeoExtendida)
                .where(qPeticionChequeoExtendida.target.toUpperCase().in("TFG", "TFM"),
                        qPeticionChequeoExtendida.estado.eq("ENVIADO"))
                .list(qPeticionChequeoExtendida.id, qPeticionChequeoExtendida.idExterno, qPeticionChequeoExtendida.asignaturaId, qPeticionChequeoExtendida.cursoAcademico)
                .stream()
                .map(IdDocumento::new)
                .collect(Collectors.toList());
    }

    /**
     * Devuelve todos los campos salvo el campo fichero.
     *
     * @param idPeticionChequeo
     * @return
     */
    public PeticionChequeo getPeticionChequeo(Long idPeticionChequeo)
    {
        JPAQuery query = new JPAQuery(this.entityManager);
        Tuple tuple =  query
                        .from(qPeticionChequeo)
                        .where(qPeticionChequeo.id.eq(idPeticionChequeo))
                        .uniqueResult(
                                qPeticionChequeo.id,
                                qPeticionChequeo.idExterno,
                                qPeticionChequeo.estado,
                                qPeticionChequeo.apellidosAutor,
                                qPeticionChequeo.fecha,
                                qPeticionChequeo.fechaUpdate,
                                qPeticionChequeo.intentos,
                                qPeticionChequeo.nombreAutor,
                                qPeticionChequeo.nombreFichero,
                                qPeticionChequeo.target,
                                qPeticionChequeo.titulo,
                                qPeticionChequeo.urlInforme,
                                qPeticionChequeo.peridAutor,
                                qPeticionChequeo.peridProfesor,
                                qPeticionChequeo.emailProfesor,
                                qPeticionChequeo.solicitudId,
                                qPeticionChequeo.referenciaFichero,
                                qPeticionChequeo.similitud
                        );

        PeticionChequeo peticionChequeo = new PeticionChequeo();
        peticionChequeo.setId(tuple.get(0, Long.class));
        peticionChequeo.setIdExterno(tuple.get(1, String.class));
        peticionChequeo.setEstado(tuple.get(2, String.class));
        peticionChequeo.setApellidosAutor(tuple.get(3, String.class));
        peticionChequeo.setFecha((tuple.get(4, Date.class)));
        peticionChequeo.setFechaUpdate(tuple.get(5, Date.class));
        peticionChequeo.setIntentos(tuple.get(6, Integer.class));
        peticionChequeo.setNombreAutor(tuple.get(7, String.class));
        peticionChequeo.setNombreFichero(tuple.get(8, String.class));
        peticionChequeo.setTarget(tuple.get(9, String.class));
        peticionChequeo.setTitulo(tuple.get(10, String.class));
        peticionChequeo.setUrlInforme(tuple.get(11, String.class));
        peticionChequeo.setPeridAutor(tuple.get(12, Long.class));
        peticionChequeo.setPeridProfesor(tuple.get(13, Long.class));
        peticionChequeo.setEmailProfesor(tuple.get(14, String.class));
        peticionChequeo.setSolicitudId(tuple.get(15, Long.class));
        peticionChequeo.setReferenciaFichero(tuple.get(16, String.class));
        peticionChequeo.setSimilitud(tuple.get(17, Long.class));

        return peticionChequeo;
    }

    /**
     * Devuelve todos los campos salvo el campo fichero.
     *
     * @param idPeticionChequeo
     * @return
     */
    public PeticionChequeoExtendida getPeticionChequeoExtendida(Long idPeticionChequeo)
    {
        JPAQuery query = new JPAQuery(this.entityManager);
        Tuple tuple =  query
                .from(qPeticionChequeoExtendida)
                .where(qPeticionChequeoExtendida.id.eq(idPeticionChequeo))
                .uniqueResult(
                        qPeticionChequeoExtendida.id,
                        qPeticionChequeoExtendida.idExterno,
                        qPeticionChequeoExtendida.estado,
                        qPeticionChequeoExtendida.apellidosAutor,
                        qPeticionChequeoExtendida.fecha,
                        qPeticionChequeoExtendida.fechaUpdate,
                        qPeticionChequeoExtendida.intentos,
                        qPeticionChequeoExtendida.nombreAutor,
                        qPeticionChequeoExtendida.nombreFichero,
                        qPeticionChequeoExtendida.target,
                        qPeticionChequeoExtendida.titulo,
                        qPeticionChequeoExtendida.urlInforme,
                        qPeticionChequeoExtendida.peridAutor,
                        qPeticionChequeoExtendida.peridProfesor,
                        qPeticionChequeoExtendida.emailProfesor,
                        qPeticionChequeoExtendida.solicitudId,
                        qPeticionChequeoExtendida.cursoAcademico,
                        qPeticionChequeoExtendida.asignaturaId,
                        qPeticionChequeoExtendida.referenciaFichero,
                        qPeticionChequeoExtendida.similitud
                );

        PeticionChequeoExtendida peticionChequeo = new PeticionChequeoExtendida();
        peticionChequeo.setId(tuple.get(0, Long.class));
        peticionChequeo.setIdExterno(tuple.get(1, String.class));
        peticionChequeo.setEstado(tuple.get(2, String.class));
        peticionChequeo.setApellidosAutor(tuple.get(3, String.class));
        peticionChequeo.setFecha((tuple.get(4, Date.class)));
        peticionChequeo.setFechaUpdate(tuple.get(5, Date.class));
        peticionChequeo.setIntentos(tuple.get(6, Integer.class));
        peticionChequeo.setNombreAutor(tuple.get(7, String.class));
        peticionChequeo.setNombreFichero(tuple.get(8, String.class));
        peticionChequeo.setTarget(tuple.get(9, String.class));
        peticionChequeo.setTitulo(tuple.get(10, String.class));
        peticionChequeo.setUrlInforme(tuple.get(11, String.class));
        peticionChequeo.setPeridAutor(tuple.get(12, Long.class));
        peticionChequeo.setPeridProfesor(tuple.get(13, Long.class));
        peticionChequeo.setEmailProfesor(tuple.get(14, String.class));
        peticionChequeo.setSolicitudId(tuple.get(15, Long.class));
        peticionChequeo.setCursoAcademico(tuple.get(16, Integer.class));
        peticionChequeo.setAsignaturaId(tuple.get(17, String.class));
        peticionChequeo.setReferenciaFichero(tuple.get(18, String.class));
        peticionChequeo.setSimilitud(tuple.get(19, Long.class));

        return peticionChequeo;
    }

    @Transactional
    public void setEstadoPeticion(Long idPeticionChequeo, String estado)
    {
        new JPAUpdateClause(entityManager, qPeticionChequeo)
                .set(qPeticionChequeo.estado, estado)
                .where(qPeticionChequeo.id.eq(idPeticionChequeo))
                .execute();
    }

    public byte[] getFichero(Long idPeticionChequeo)
    {
        return entityManager.createQuery("SELECT p.fichero FROM PeticionChequeo p WHERE p.id = :id", byte[].class)
                .setParameter("id", idPeticionChequeo)
                .getSingleResult();
    }

    public boolean existePeticionChequeo(Long idPeticionChequeo)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query
                .from(qPeticionChequeoExtendida)
                .where(qPeticionChequeoExtendida.id.eq(idPeticionChequeo),
                       qPeticionChequeoExtendida.target.toUpperCase().in("TFG", "TFM")
                )
                .exists();
    }

    @Transactional
    public Integer incrementarNumeroDeIntentos(Long idPeticionChequeo)
    {
        Integer intentos = this.obtenerNumeroDeIntentos(idPeticionChequeo);
        new JPAUpdateClause(entityManager, qPeticionChequeo)
                .set(qPeticionChequeo.intentos, intentos + 1)
                .where(qPeticionChequeo.id.eq(idPeticionChequeo))
                .execute();

        return intentos + 1;
    }

    /**
     * Obtiene el número de veces que se ha intentado hacer un chequeo.
     *
     * @param idPeticionChequeo
     * @return
     */
    public Integer obtenerNumeroDeIntentos(Long idPeticionChequeo)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qPeticionChequeo).where(qPeticionChequeo.id.eq(idPeticionChequeo)).uniqueResult(qPeticionChequeo.intentos);
    }

    /**
     * La implementación de este método es un poco sui generis. Si actualizo toda la
     * entidad, se carga el fichero almacenado. Así es que, como sólo puedo actualizar
     * el estado, idExterno y la url, sólo actualizo esos campos.
     *
     * @param peticion
     */
    @Transactional
    public void actualizarPeticionChequeo (PeticionChequeo peticion) {
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qPeticionChequeo);

        if (peticion.getEstado() != null) {
            updateClause.set(qPeticionChequeo.estado, peticion.getEstado());
        }
        if (peticion.getIdExterno() != null) {
            updateClause.set(qPeticionChequeo.idExterno, peticion.getIdExterno());
        }
        if (peticion.getUrlInforme() != null) {
            updateClause.set(qPeticionChequeo.urlInforme, peticion.getUrlInforme());
        }
        if (peticion.getSimilitud() != null) {
            updateClause.set(qPeticionChequeo.similitud, peticion.getSimilitud());
        }

        updateClause.where(qPeticionChequeo.id.eq(peticion.getId()))
                .execute();
    }

    /**
     * Dado un id de SPI, devuelve una lista de profesorado que debe tener acceso a un informe antiplagio.
     *
     * @param spiId El id de SPI.
     * @return Lista de perids del profesorado.
     */
    public List<Long> obtenerProfesores (Long spiId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qProfesorTFG)
                .where(qProfesorTFG.spiId.eq(spiId))
                .list(qProfesorTFG)
                .stream()
                .map(ProfesorTFG::getPersonaId)
                .collect(Collectors.toList());
    }
}
