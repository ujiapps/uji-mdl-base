package es.uji.apps.mdl.base;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class StreamUtils
{
    public static void stream(InputStream input, OutputStream output)
            throws IOException
    {
        byte[] buffer = new byte[102400];

        for (int length = 0; (length = input.read(buffer)) > 0; )
        {
            output.write(buffer, 0, length);
        }

        input.close();
        output.close();
    }
}
