package es.uji.apps.mdl.base.model;

import java.util.Date;

import javax.persistence.Id;

public class FichaCompletaLPI
{
    @Id
    private String id;

    private String titulo;
    private String autor;
    private String editorial;
    private String identificacion;
    private String url;
    private String paginas;
    private Integer totalPaginas;
    private Integer totalMatriculados;
    private String usuarioSubidaFichero;
    private String usuarioRellenaMetadatos;
    private Date fechaSubida;
    private Date fechaRellenado;
    private String urlDescarga;
    private String contentTypeFile;
    private String nameFile;
    private Long sizeFile;
    private String estado;
    private String comentarios;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getTitulo()
    {
        return titulo;
    }

    public void setTitulo(String titulo)
    {
        this.titulo = titulo;
    }

    public String getAutor()
    {
        return autor;
    }

    public void setAutor(String autor)
    {
        this.autor = autor;
    }

    public String getEditorial()
    {
        return editorial;
    }

    public void setEditorial(String editorial)
    {
        this.editorial = editorial;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public Integer getTotalPaginas()
    {
        return totalPaginas;
    }

    public void setTotalPaginas(Integer totalPaginas)
    {
        this.totalPaginas = totalPaginas;
    }

    public Integer getTotalMatriculados()
    {
        return totalMatriculados;
    }

    public void setTotalMatriculados(Integer totalMatriculados)
    {
        this.totalMatriculados = totalMatriculados;
    }

    public String getUsuarioSubidaFichero()
    {
        return usuarioSubidaFichero;
    }

    public void setUsuarioSubidaFichero(String usuarioSubidaFichero)
    {
        this.usuarioSubidaFichero = usuarioSubidaFichero;
    }

    public String getUsuarioRellenaMetadatos()
    {
        return usuarioRellenaMetadatos;
    }

    public void setUsuarioRellenaMetadatos(String usuarioRellenaMetadatos)
    {
        this.usuarioRellenaMetadatos = usuarioRellenaMetadatos;
    }

    public Date getFechaSubida()
    {
        return fechaSubida;
    }

    public void setFechaSubida(Date fechaSubida)
    {
        this.fechaSubida = fechaSubida;
    }

    public Date getFechaRellenado()
    {
        return fechaRellenado;
    }

    public void setFechaRellenado(Date fechaRellenado)
    {
        this.fechaRellenado = fechaRellenado;
    }

    public String getUrlDescarga()
    {
        return urlDescarga;
    }

    public void setUrlDescarga(String urlDescarga)
    {
        this.urlDescarga = urlDescarga;
    }

    public String getContentTypeFile()
    {
        return contentTypeFile;
    }

    public void setContentTypeFile(String contentTypeFile)
    {
        this.contentTypeFile = contentTypeFile;
    }

    public String getNameFile()
    {
        return nameFile;
    }

    public void setNameFile(String nameFile)
    {
        this.nameFile = nameFile;
    }

    public Long getSizeFile()
    {
        return sizeFile;
    }

    public void setSizeFile(Long sizeFile)
    {
        this.sizeFile = sizeFile;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public String getIdentificacion()
    {
        return identificacion;
    }

    public void setIdentificacion(String identificacion)
    {
        this.identificacion = identificacion;
    }

    public String getPaginas()
    {
        return paginas;
    }

    public void setPaginas(String paginas)
    {
        this.paginas = paginas;
    }

    public String getComentarios()
    {
        return comentarios;
    }

    public void setComentarios(String comentarios)
    {
        this.comentarios = comentarios;
    }
}

