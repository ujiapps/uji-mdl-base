package es.uji.apps.mdl.base.dao;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.mdl.base.model.antiplagio.PeticionChequeo;
import es.uji.apps.mdl.base.model.borrado.EstadoBorrado;
import es.uji.apps.mdl.base.model.borrado.PeticionBorrado;
import es.uji.apps.mdl.base.model.borrado.PeticionBorradoLog;
import es.uji.apps.mdl.base.model.borrado.QPeticionBorrado;
import es.uji.apps.mdl.base.model.borrado.QPeticionBorradoLog;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class BorradoCursosDAO extends BaseDAODatabaseImpl
{
    QPeticionBorrado qPeticionBorrado = QPeticionBorrado.peticionBorrado;
    QPeticionBorradoLog qPeticionBorradoLog = QPeticionBorradoLog.peticionBorradoLog;

    /**
     * Devuelve un listado de peticiones de borrado de curso en un estado dado.
     *
     * @param estado El estado en el que se encuentra la petición.
     * @return Lista de peticiones en el estado indicado.
     */
    public List<PeticionBorrado> getPeticionesEnEstado (EstadoBorrado estado)
    {
        JPAQuery query = new JPAQuery(this.entityManager);
        return query.from(qPeticionBorrado).where(qPeticionBorrado.estado.eq(estado.toString())).list(qPeticionBorrado);
    }

    @Transactional
    public PeticionBorrado setEstado (PeticionBorrado peticionBorrado, EstadoBorrado nuevoEstado, String info)
    {
        PeticionBorrado nuevaPeticion = new PeticionBorrado();
        nuevaPeticion.setId(peticionBorrado.getId());
        nuevaPeticion.setEstado(nuevoEstado.toString());
        nuevaPeticion.setFechaSolicitud(peticionBorrado.getFechaSolicitud());
        nuevaPeticion.setPmeId(peticionBorrado.getPmeId());
        nuevaPeticion.setSolicitanteId(peticionBorrado.getSolicitanteId());
        nuevaPeticion.setInfo(info);

        nuevaPeticion = this.update(nuevaPeticion);

        PeticionBorradoLog log = new PeticionBorradoLog();
        log.setEstado(nuevoEstado.toString());
        log.setFechaEstado(LocalDateTime.now());
        log.setPeticionBorrado(nuevaPeticion);
        log.setInfo(info);

        this.insert(log);

        return nuevaPeticion;
    }
}
