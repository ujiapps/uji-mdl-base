package es.uji.apps.mdl.base.prodigital.exceptions;

public class ValueNotAllowedException extends ProdigitalException {
    public ValueNotAllowedException(String field, String value) {
        super(500, "El valor " + value + " no es válido para el campo " + field, "valuenotallowed");
    }
}
