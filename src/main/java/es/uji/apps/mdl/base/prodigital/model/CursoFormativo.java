package es.uji.apps.mdl.base.prodigital.model;

import javax.persistence.*;

@Entity
@Table(name = "prodigital_curso_formativo_json")
public class CursoFormativo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "curso_formativo")
    protected String cursoFormativo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCursoFormativo() {
        return cursoFormativo;
    }

    public void setCursoFormativo(String cursoFormativo) {
        this.cursoFormativo = cursoFormativo;
    }
}
