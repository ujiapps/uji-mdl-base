package es.uji.apps.mdl.base.prodigital.exceptions;

public class MissingRequiredFieldException extends ProdigitalException {
    public MissingRequiredFieldException(String missingParam) {
        super(520, "El campo obligatorio " + missingParam + " no está presente en la petición", "missingrequiredfielderror");
    }
}
