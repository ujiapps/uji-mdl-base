package es.uji.apps.mdl.base.model.estadisticas;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "gra_mdl.mdl_log_accesos")
public class MdlLogAccesos {

    @Column
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column
    LocalDateTime fecha;

    @Column
    String texto;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public void setFecha(LocalDateTime fecha) {
        this.fecha = fecha;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
}
