package es.uji.apps.mdl.base.services;

import es.uji.apps.mdl.base.dao.EstadisticasDAO;
import es.uji.apps.mdl.base.dto.estadisticas.*;
import es.uji.apps.mdl.base.model.estadisticas.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EstadisticasService {

    @Autowired
    EstadisticasDAO estadisticasDAO;

    public void borrarTablasDeCarga() {
        this.estadisticasDAO.borrarTablasCarga();
    }

    public void insertarUsers(List<MdlUserDTO> userDTOList) {
        List<MdlUser> models = userDTOList.stream().map((userDTO) -> {
            MdlUser modelUser = new MdlUser();
            modelUser.setUsername(userDTO.getUsername());
            modelUser.setLastLogin(userDTO.getLastLogin());
            return modelUser;
        }).collect(Collectors.toList());

        this.estadisticasDAO.insertarUsers(models);
    }

    public void insertarCourses(List<MdlCourseDTO> courseDTOList) {
        List<MdlCourse> models = courseDTOList.stream().map((courseDTO) -> {
            MdlCourse model = new MdlCourse();
            model.setOraCourseId(courseDTO.getOraCourseId());
            model.setFullname(courseDTO.getFullname());
            model.setProcesar(courseDTO.getProcesar());
            model.setShortname(courseDTO.getShortname());
            model.setVisible(courseDTO.getVisible());
            return model;
        }).collect(Collectors.toList());

        this.estadisticasDAO.insertarCourses(models);
    }

    public void insertarUserStudents(List<MdlUserStudentsDTO> userStudentsDTOList) {
        List<MdlUserStudents> models = userStudentsDTOList.stream().map((userStudentsDTO) -> {
            MdlUserStudents model = new MdlUserStudents();
            model.setAsistente(userStudentsDTO.getAsistente());
            model.setUsername(userStudentsDTO.getUsername());
            model.setOraCorseId(userStudentsDTO.getOraCourseId());
            return model;
        }).collect(Collectors.toList());

        this.estadisticasDAO.insertarMdlUserStudents(models);
    }

    public void insertarUserTeachers(List<MdlUserTeachersDTO> userDTOList) {
        List<MdlUserTeachers> models = userDTOList.stream().map((userDTO) -> {
            MdlUserTeachers model = new MdlUserTeachers();
            model.setUsername(userDTO.getUsername());
            model.setOraCourseId(userDTO.getOraCourseId());
            return model;
        }).collect(Collectors.toList());

        this.estadisticasDAO.insertarMdlUserTeachers(models);
    }

    @Transactional
    public void insertarAccesos(AccesosDTO courses) {

        MdlLogAccesos mdlLogAccesos = new MdlLogAccesos();
        mdlLogAccesos.setFecha(LocalDateTime.ofEpochSecond(courses.getTimestamp(),0, ZoneOffset.UTC));
        mdlLogAccesos.setTexto(courses.getCurso() + " " +  mdlLogAccesos.getFecha().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + " " + courses.getId() + " " + courses.getValor());

        this.estadisticasDAO.insertarMdlLogAccesos(mdlLogAccesos);

        PodMoodleAccesos podMoodleAccesos = new PodMoodleAccesos();
        podMoodleAccesos.setCurso(courses.getCurso());
        podMoodleAccesos.setFecha(LocalDateTime.ofEpochSecond(courses.getTimestamp(),0, ZoneOffset.UTC));
        podMoodleAccesos.setId(courses.getId());
        podMoodleAccesos.setValor(courses.getValor());

        this.estadisticasDAO.insertarPodMoodleAccesos(podMoodleAccesos);
    }

    /**
     * Obtiene los ids de cursos (pod_moodle) creados en el curso académico indicado.
     *
     * @param cursoAcademico
     * @return
     */
    public List<Long> obtenerCursos (int cursoAcademico) {
        return this.estadisticasDAO.obtenerCursos(cursoAcademico);
    }
}
