package es.uji.apps.mdl.base.model.estadisticas;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@IdClass(PodMoodleAccesosId.class)
@Table( name = "gra_pod.pod_moodle_accesos")
public class PodMoodleAccesos {

    @Id
    @Column
    Integer curso;

    @Id
    @Column
    LocalDateTime fecha;

    @Id
    @Column
    Long id;

    @Column
    Long valor;

    public Integer getCurso() {
        return curso;
    }

    public void setCurso(Integer curso) {
        this.curso = curso;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public void setFecha(LocalDateTime fecha) {
        this.fecha = fecha;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getValor() {
        return valor;
    }

    public void setValor(Long valor) {
        this.valor = valor;
    }
}
