package es.uji.apps.mdl.base.prodigital.dto;

public class ErrorDTO {

    protected boolean error;

    protected String codigoHttp;

    protected String codigoError;

    protected String descripcionError;

    public boolean getError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getCodigoHttp() {
        return codigoHttp;
    }

    public void setCodigoHttp(String codigoHttp) {
        this.codigoHttp = codigoHttp;
    }

    public String getCodigoError() {
        return codigoError;
    }

    public void setCodigoError(String codigoError) {
        this.codigoError = codigoError;
    }

    public String getDescripcionError() {
        return descripcionError;
    }

    public void setDescripcionError(String descripcionError) {
        this.descripcionError = descripcionError;
    }
}
