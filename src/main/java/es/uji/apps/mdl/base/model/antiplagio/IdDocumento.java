package es.uji.apps.mdl.base.model.antiplagio;

import com.mysema.query.Tuple;

public class IdDocumento
{
    private Long id;
    private String idExterno;

    public IdDocumento (Tuple tupla) {
        this.id = tupla.get(0, Long.class);
        this.idExterno = tupla.get(1, String.class);
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getIdExterno()
    {
        return idExterno;
    }

    public void setIdExterno(String idExterno)
    {
        this.idExterno = idExterno;
    }
}
