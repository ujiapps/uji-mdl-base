package es.uji.apps.mdl.base.model.borrado;

public enum EstadoBorrado
{
    PENDIENTE("PENDIENTE"),
    BACKUP_INICIADO("BACKUP_INICIADO"),
    BACKUP_FINALIZADO("BACKUP_FINALIZADO"),
    BORRADO_EN_ORACLE("BORRADO_EN_ORACLE"),
    BORRADO_EN_LMS_INICIADO("BORRADO_EN_LMS_INICIADO"),
    BORRADO_EN_LMS_FINALIZADO("BORRADO_EN_LMS_FINALIZADO"),
    FINALIZADO("FINALIZADO"),
    ERROR("ERROR"),
    ;

    private final String estado;

    EstadoBorrado(String estado)
    {
        this.estado = estado;
    }

    @Override
    public String toString()
    {
        return this.estado;
    }
}
