package es.uji.apps.mdl.base.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.mdl.base.dto.antiplagio.IdDocumentoDTO;
import es.uji.apps.mdl.base.dto.antiplagio.PeticionChequeoDTO;
import es.uji.apps.mdl.base.exceptions.PeticionNotFoundException;
import es.uji.apps.mdl.base.model.antiplagio.EstadoChequeoAntiplagio;
import es.uji.apps.mdl.base.responses.RespuestaGeneral;
import es.uji.apps.mdl.base.services.AntiplagioService;
import org.springframework.web.bind.annotation.RequestBody;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Path("antiplagio")
public class AntiplagioResource {
    @InjectParam
    AntiplagioService antiplagioService;

    @GET
    @Path("/peticion")
    @Produces(MediaType.APPLICATION_JSON)
    public RespuestaGeneral<IdDocumentoDTO> obtenerPeticiones(
            @DefaultValue("pendiente") @QueryParam("estado") String estado
    ) {
        List<IdDocumentoDTO> peticiones = new ArrayList<>(0);
        switch (EstadoChequeoAntiplagio.valueOf(estado.toUpperCase())) {
            case PENDIENTE:
                peticiones = antiplagioService.obtenerPeticionesPendientesDeEnvio();
                break;
            case ENVIADO:
                peticiones = antiplagioService.obtenerPeticionesEnviadas();
                break;
        }
        return new RespuestaGeneral<>(true, peticiones);
    }

    @GET
    @Path("/peticion/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response obtenerPeticion(@PathParam("id") Long idPeticionChequeo) {

        Response response;
        PeticionChequeoDTO peticionDTO;

        try {
            peticionDTO = antiplagioService.obtenerPeticionExtendida(idPeticionChequeo);

            response = Response
                    .status(Response.Status.OK)
                    .entity(new RespuestaGeneral<PeticionChequeoDTO>(true, Arrays.asList(peticionDTO)))
                    .build();

        } catch (PeticionNotFoundException e) {
            response = Response
                    .status(Response.Status.NOT_FOUND)
                    .entity(new RespuestaGeneral<PeticionChequeoDTO>(false, new ArrayList<>(0)))
                    .build();
        }

        return response;
    }

    @PUT
    @Path("/peticion/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizarPeticion(@RequestBody PeticionChequeoDTO peticionChequeo) {
        Response response;

        try {
            PeticionChequeoDTO dto = antiplagioService.actualizarPeticionChequeo(peticionChequeo);
            response = Response.status(Response.Status.OK)
                    .entity(new RespuestaGeneral<>(true, Arrays.asList(dto)))
                    .build();
        } catch (PeticionNotFoundException e) {
            response = Response.status(Response.Status.NOT_FOUND)
                    .entity(new RespuestaGeneral<>(true, new ArrayList<>(0)))
                    .build();
        }
        return response;
    }

    @GET
    @Path("/peticion/{id}/fichero")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response obtenerFicheroDePeticion(@PathParam("id") Long idPeticionChequeo) {
        Response response;

        try {
            byte[] fichero = antiplagioService.obtenerFichero(idPeticionChequeo);

            response = Response
                    .status(Response.Status.OK)
                    .entity(fichero)
                    .build();
        } catch (PeticionNotFoundException e) {
            response = Response
                    .status(Response.Status.NOT_FOUND)
                    .entity(new byte[0])
                    .build();
        }

        return response;
    }

    @GET
    @Path("/peticion/{id}/profesores")
    @Produces(MediaType.APPLICATION_JSON)
    public Response obtenerProfesores(@PathParam("id") Long idPeticionChequeo) {

        Response response;
        List<Long> profesores;
        try {
            profesores = antiplagioService.obtenerProfesores(idPeticionChequeo);
            response = Response
                    .status(Response.Status.OK)
                    .entity(new RespuestaGeneral<Long>(true, profesores))
                    .build();

        } catch (PeticionNotFoundException e) {
            response = Response
                    .status(Response.Status.NOT_FOUND)
                    .entity(new RespuestaGeneral<Long>(false, new ArrayList<>(0)))
                    .build();
        }

        return response;
    }
}
