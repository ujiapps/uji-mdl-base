package es.uji.apps.mdl.base.responses;

import java.util.Collections;

public class RespuestaErronea<T> extends RespuestaGeneral<T>
{
    private String errorMessage;

    public RespuestaErronea(Exception e)
    {
        super(false, Collections.emptyList());
        this.errorMessage = e.getMessage();
    }
}
