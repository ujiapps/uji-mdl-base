package es.uji.apps.mdl.base.prodigital.dto;

public class AuditoriasDTO {

    protected String autor;

    protected String fechaModificacion;

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(String fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }
}
