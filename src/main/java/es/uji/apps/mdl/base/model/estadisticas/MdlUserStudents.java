package es.uji.apps.mdl.base.model.estadisticas;

import javax.persistence.*;

@Entity
@Table(name = "gra_mdl.mdl_user_students")
public class MdlUserStudents {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @Column(name = "username")
    private String username;

    @Column(name = "ora_courseid")
    private Long oraCorseId;

    @Column(name = "asistente")
    private String asistente;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getOraCorseId() {
        return oraCorseId;
    }

    public void setOraCorseId(Long oraCorseId) {
        this.oraCorseId = oraCorseId;
    }

    public String getAsistente() {
        return asistente;
    }

    public void setAsistente(String asistente) {
        this.asistente = asistente;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }
}
