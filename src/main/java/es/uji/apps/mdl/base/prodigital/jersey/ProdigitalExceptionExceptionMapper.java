package es.uji.apps.mdl.base.prodigital.jersey;

import es.uji.apps.mdl.base.prodigital.dto.ProdigitalExceptionDTO;
import es.uji.apps.mdl.base.prodigital.exceptions.ProdigitalException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ProdigitalExceptionExceptionMapper implements ExceptionMapper<ProdigitalException> {
    @Override
    public Response toResponse(ProdigitalException e) {
        return Response
                .status(e.getCodigoHttp())
                .type(MediaType.APPLICATION_JSON)
                .entity(new ProdigitalExceptionDTO(e))
                .build();
    }
}
