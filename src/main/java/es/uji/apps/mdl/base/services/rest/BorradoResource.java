package es.uji.apps.mdl.base.services.rest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.web.bind.annotation.RequestBody;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.mdl.base.dto.borrado.PeticionBorradoDTO;
import es.uji.apps.mdl.base.exceptions.PeticionNotFoundException;
import es.uji.apps.mdl.base.model.borrado.EstadoBorrado;
import es.uji.apps.mdl.base.responses.RespuestaGeneral;
import es.uji.apps.mdl.base.services.BorradoService;

@Path("borrado")
public class BorradoResource
{
    @InjectParam
    BorradoService borradoService;

    @GET
    @Path("/solicitudes")
    @Produces(MediaType.APPLICATION_JSON)
    public RespuestaGeneral<PeticionBorradoDTO> obtenerSolicitudes(
            @QueryParam("estado") EstadoBorrado estado
    )
    {
        List<PeticionBorradoDTO> peticionesBorrado = borradoService.obtenerPeticionesEnEstado(estado);
        return new RespuestaGeneral<PeticionBorradoDTO>(true, peticionesBorrado);
    }

    @PUT
    @Path("/solicitud/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizarSolicitud(@PathParam("id") Long solicitudId, @RequestBody PeticionBorradoDTO peticionChequeo) {
        Response response;

        if (!solicitudId.equals(peticionChequeo.getId()))
        {
            return Response.status(Response.Status.NOT_ACCEPTABLE)
                    .entity(new RespuestaGeneral<>(false, new ArrayList<>(0)))
                    .build();
        }

        try {
            PeticionBorradoDTO dto = borradoService.actualizarEstado(peticionChequeo);
            response = Response.status(Response.Status.OK)
                    .entity(new RespuestaGeneral<>(true, Arrays.asList(dto)))
                    .build();
        } catch (PeticionNotFoundException e) {
            response = Response.status(Response.Status.NOT_FOUND)
                    .entity(new RespuestaGeneral<>(true, new ArrayList<>(0)))
                    .build();
        }
        return response;
    }

}
