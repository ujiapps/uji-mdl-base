package es.uji.apps.mdl.base.model.borrado;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "gra_pod.pod_moodle_borrado_estados_log")
public class PeticionBorradoLog
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "pmb_id")
    private PeticionBorrado peticionBorrado;

    @Column(name = "fecha_estado")
    private LocalDateTime fechaEstado;

    @Column(name = "estado")
    private String estado;

    @Column(name = "info")
    private String info;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public PeticionBorrado getPeticionBorrado()
    {
        return peticionBorrado;
    }

    public void setPeticionBorrado(PeticionBorrado peticionBorrado)
    {
        this.peticionBorrado = peticionBorrado;
    }

    public LocalDateTime getFechaEstado()
    {
        return fechaEstado;
    }

    public void setFechaEstado(LocalDateTime fechaEstado)
    {
        this.fechaEstado = fechaEstado;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public String getInfo()
    {
        return info;
    }

    public void setInfo(String info)
    {
        this.info = info;
    }
}
