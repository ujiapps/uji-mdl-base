package es.uji.apps.mdl.base.dto.estadisticas;

public class MdlUserTeachersDTO {

    private String username;

    private Long oraCourseId;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getOraCourseId() {
        return oraCourseId;
    }

    public void setOraCourseId(Long oraCourseId) {
        this.oraCourseId = oraCourseId;
    }
}
