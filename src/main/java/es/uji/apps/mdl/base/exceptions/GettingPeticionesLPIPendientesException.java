package es.uji.apps.mdl.base.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

public class GettingPeticionesLPIPendientesException extends CoreDataBaseException
{
    public GettingPeticionesLPIPendientesException()
    {
        super("Errada recuperant la llista de peticiones pendents");
    }

    public GettingPeticionesLPIPendientesException(String message)
    {
        super(message);
    }
}
