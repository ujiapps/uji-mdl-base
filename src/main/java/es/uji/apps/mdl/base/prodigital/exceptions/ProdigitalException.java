package es.uji.apps.mdl.base.prodigital.exceptions;

public class ProdigitalException extends Exception {

    protected int codigoHttp;
    protected String descripcionError;
    protected String codigoError;

    public ProdigitalException(int codigoHttp, String descripcionError, String codigoError) {
        super(descripcionError);

        this.codigoHttp = codigoHttp;
        this.descripcionError = descripcionError;
        this.codigoError = codigoError;
    }

    public ProdigitalException(int codigoHttp, String descripcionError, String codigoError, Exception throwable) {
        super(descripcionError, throwable);

        this.codigoHttp = codigoHttp;
        this.descripcionError = descripcionError;
        this.codigoError = codigoError;
    }

    public int getCodigoHttp() {
        return codigoHttp;
    }

    public String getDescripcionError() {
        return descripcionError;
    }

    public String getCodigoError() {
        return codigoError;
    }
}
