package es.uji.apps.mdl.base.prodigital.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.mdl.base.prodigital.model.CursoFormativo;
import es.uji.apps.mdl.base.prodigital.model.QCursoFormativo;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class ProdigitalDAO extends BaseDAODatabaseImpl {
    private static QCursoFormativo qCursoFormativo = QCursoFormativo.cursoFormativo1;

    @Transactional
    public void almacenarCursoFormativo (CursoFormativo cursoFormativo) {
        this.entityManager.persist(cursoFormativo);
    }

    public CursoFormativo getCursoFormativoById (Long id) {
        JPAQuery q = new JPAQuery(this.entityManager);
        return q.from(qCursoFormativo).where(qCursoFormativo.id.eq(id)).singleResult(qCursoFormativo);
    }
}
