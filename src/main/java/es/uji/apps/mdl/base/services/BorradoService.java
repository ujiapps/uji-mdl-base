package es.uji.apps.mdl.base.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.mdl.base.dao.BorradoCursosDAO;
import es.uji.apps.mdl.base.dto.borrado.PeticionBorradoDTO;
import es.uji.apps.mdl.base.exceptions.PeticionNotFoundException;
import es.uji.apps.mdl.base.model.borrado.EstadoBorrado;
import es.uji.apps.mdl.base.model.borrado.PeticionBorrado;

@Service
public class BorradoService
{
    @Autowired
    BorradoCursosDAO borradoCursosDAO;

    public List<PeticionBorradoDTO> obtenerPeticionesEnEstado (EstadoBorrado estado)
    {
        List<PeticionBorrado> peticiones = borradoCursosDAO.getPeticionesEnEstado(estado);
        return PeticionBorradoDTO.toDTO(peticiones);
    }

    /**
     * Actualiza el estado de borrado de una petición.
     *
     * @param peticionBorrado Petición de borrado.
     */
    public PeticionBorradoDTO actualizarEstado (PeticionBorradoDTO peticionBorrado) throws PeticionNotFoundException
    {
        PeticionBorrado peticionBorradoActual = (PeticionBorrado) borradoCursosDAO.get(PeticionBorrado.class, peticionBorrado.getId()).get(0);
        if (peticionBorradoActual == null )
        {
            throw new PeticionNotFoundException(peticionBorrado.getId());
        }

        peticionBorradoActual = borradoCursosDAO.setEstado(peticionBorradoActual, EstadoBorrado.valueOf(peticionBorrado.getEstado()), peticionBorrado.getInfo());

        return PeticionBorradoDTO.toDTO(peticionBorradoActual);
    }

}
