package es.uji.apps.mdl.base.dto.estadisticas;


public class MdlUserStudentsDTO {

    private String username;
    private Long oraCourseId;
    private String asistente;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getOraCourseId() {
        return oraCourseId;
    }

    public void setOraCourseId(Long oraCourseId) {
        this.oraCourseId = oraCourseId;
    }

    public String getAsistente() {
        return asistente;
    }

    public void setAsistente(String asistente) {
        this.asistente = asistente;
    }
}
