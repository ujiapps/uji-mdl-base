package es.uji.apps.mdl.base.model.estadisticas;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

public class PodMoodleAccesosId implements Serializable {

    Integer curso;
    LocalDateTime fecha;
    Long id;


    public PodMoodleAccesosId() {
        this.curso = null;
        this.fecha = null;
        this.id = null;
    }

    public PodMoodleAccesosId(Integer curso, LocalDateTime fecha, Long id) {
        this.curso = curso;
        this.fecha = fecha;
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PodMoodleAccesosId that = (PodMoodleAccesosId) o;
        return Objects.equals(curso, that.curso) && Objects.equals(fecha, that.fecha) && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(curso, fecha, id);
    }
}
