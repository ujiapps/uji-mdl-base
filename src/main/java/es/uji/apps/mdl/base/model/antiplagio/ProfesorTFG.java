package es.uji.apps.mdl.base.model.antiplagio;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ANTIPLAGIO_V_PROF_TFG")
public class ProfesorTFG implements Serializable
{
    @Id
    @Column(name = "sol_id")
    private Long spiId;

    @Id
    @Column(name = "per_id")
    private Long personaId;

    @Id
    @Column(name = "curso_aca")
    private Long cursoAcademico;

    public Long getSpiId()
    {
        return spiId;
    }

    public void setSpiId(Long spiId)
    {
        this.spiId = spiId;
    }

    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public Long getCursoAcademico()
    {
        return cursoAcademico;
    }

    public void setCursoAcademico(Long cursoAcademico)
    {
        this.cursoAcademico = cursoAcademico;
    }
}
