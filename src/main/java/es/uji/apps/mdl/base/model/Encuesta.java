package es.uji.apps.mdl.base.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "grs_faq.faq_en2_vw_encuestas_av")
public class Encuesta
{
    @Id
    @Column(name = "PER_ID")
    private Long perid;

    @Column(name = "MASTER")
    private String master;

    @Column(name = "GRADO")
    private String grado;


    public Long getPerid()
    {
        return perid;
    }

    public void setPerid(Long perid)
    {
        this.perid = perid;
    }

    public String getMaster()
    {
        return master;
    }

    public void setMaster(String master)
    {
        this.master = master;
    }

    public String getGrado()
    {
        return grado;
    }

    public void setGrado(String grado)
    {
        this.grado = grado;
    }
}
