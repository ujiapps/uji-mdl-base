package es.uji.apps.mdl.base.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

public class StateNotAllowedException extends CoreDataBaseException
{
    public StateNotAllowedException()
    {
        super("Aquest estat d'aprovació no està permés. Consulteu amb l'Administrador.");
    }

    public StateNotAllowedException(String message)
    {
        super(message);
    }
}
