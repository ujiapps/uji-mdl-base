package es.uji.apps.mdl.base.services.rest;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.mdl.base.StreamUtils;
import es.uji.apps.mdl.base.exceptions.GettingFileException;
import es.uji.apps.mdl.base.exceptions.GettingPeticionLPIException;
import es.uji.apps.mdl.base.exceptions.GettingPeticionesLPIPendientesException;
import es.uji.apps.mdl.base.exceptions.SettingStatePeticionLPIException;
import es.uji.apps.mdl.base.exceptions.StateNotAllowedException;
import es.uji.apps.mdl.base.model.Estado;
import es.uji.apps.mdl.base.model.FichaCompletaLPI;
import es.uji.apps.mdl.base.model.PeticionesPaginadasLPI;
import es.uji.apps.mdl.base.services.PeticionLPIService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

@Path("peticioneslpi")
public class PeticionLPIResource
{
    @InjectParam
    PeticionLPIService peticionLPIService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getPeticionesLPI(@QueryParam("start") Integer start, @QueryParam("limit") Integer limit,
                                            @QueryParam("query") String query)
            throws GettingPeticionesLPIPendientesException
    {
        PeticionesPaginadasLPI peticiones = peticionLPIService.getByQuery(start, limit, query);

        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.setSuccess(true);

        responseMessage.setData(UIEntity.toUI(peticiones.getPeticiones()));
        responseMessage.setTotalCount(peticiones.getTotal());

        return responseMessage;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{peticionId}")
    public UIEntity getPeticionLPI(@PathParam("peticionId") String peticionId)
            throws GettingPeticionLPIException
    {
        return UIEntity.toUI(peticionLPIService.get(peticionId));
    }

    @GET
    @Path("{peticionId}/file")
    public void getFile(@PathParam("peticionId") String peticionId, @Context HttpServletResponse response)
            throws GettingPeticionLPIException, IOException, GettingFileException
    {
        FichaCompletaLPI fichaCompletaLPI = peticionLPIService.get(peticionId);

        InputStream contentFile = peticionLPIService.getFileByUrl(fichaCompletaLPI.getUrlDescarga());

        response.setContentType(fichaCompletaLPI.getContentTypeFile());
        response.setHeader("Content-disposition", "attachment; filename=\"" + fichaCompletaLPI.getNameFile() + "\"");

        StreamUtils.stream(contentFile, response.getOutputStream());
    }

    @PUT
    @Path("{peticionId}/aprobar")
    public void accept(@PathParam("peticionId") String peticionId, UIEntity uiEntity)
            throws SettingStatePeticionLPIException, ParseException, StateNotAllowedException
    {
        String fechaFinLicencia = uiEntity.get("fechaFinLicencia");
        String estadoAprobacion = uiEntity.get("estadoAprobacion");

        if (!Estado.contains(estadoAprobacion))
        {
            throw new StateNotAllowedException();
        }

        peticionLPIService.aprobar(peticionId, Estado.valueOf(estadoAprobacion).value(), fechaFinLicencia);
    }

    @PUT
    @Path("{peticionId}/denegar")
    public void deny(@PathParam("peticionId") String peticionId, UIEntity uiEntity)
            throws SettingStatePeticionLPIException
    {
        String motivoDenegacion = uiEntity.get("motivoDenegacion");

        peticionLPIService.denegar(peticionId, motivoDenegacion);
    }
}

