package es.uji.apps.mdl.base.dao;

import es.uji.apps.mdl.base.dto.enrol.CreatedCourseDTO;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPAUpdateClause;

import es.uji.apps.mdl.base.model.enrol.QCourse;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class EnrolDAO extends BaseDAODatabaseImpl
{
    private QCourse qCourse = QCourse.course;

    @Transactional
    public void markCourseAsCreated(Long courseId)
    {
        new JPAUpdateClause(this.entityManager, this.qCourse).where(qCourse.id.eq(courseId)).set(qCourse.sincronizado, "S")
                .execute();
    }

    @Transactional
    public void markCourseAsCreated(CreatedCourseDTO course)
    {
        JPAUpdateClause updateClause = new JPAUpdateClause(this.entityManager, this.qCourse);
        updateClause.where(
                qCourse.id.eq(course.getCourseidnumber())
                ).set(qCourse.sincronizado, "S").set(qCourse.idmoodle, course.getCourseid())
                .execute();
    }

}
