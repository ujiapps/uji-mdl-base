package es.uji.apps.mdl.base.dto.antiplagio;

import com.google.gson.Gson;
import es.uji.apps.mdl.base.model.antiplagio.PeticionChequeo;
import es.uji.apps.mdl.base.model.antiplagio.PeticionChequeoExtendida;

import java.util.Date;

public class PeticionChequeoDTO
{
    private Long id;
    private String estado;
    private String urlInforme;
    private String nombreFichero;
    private String nombreAutor;
    private String apellidosAutor;
    private String titulo;
    private Date fecha;
    private Date fechaUpdate;
    private String target;
    private String idExterno;
    private Long peridAutor;
    private Long peridProfesor;
    private Integer cursoAcademico;
    private String asignaturaId;
    private String referenciaFichero;
    private String emailProfesor;
    private Long score;

    public static PeticionChequeoDTO toDTO (PeticionChequeoExtendida peticionChequeo) {
        PeticionChequeoDTO dto = new PeticionChequeoDTO();
        dto.setApellidosAutor(peticionChequeo.getApellidosAutor());
        dto.setId(peticionChequeo.getId());
        dto.setIdExterno(peticionChequeo.getIdExterno());
        dto.setNombreAutor(peticionChequeo.getNombreAutor());
        dto.setTitulo(peticionChequeo.getTitulo());
        dto.setUrlInforme(peticionChequeo.getUrlInforme());
        dto.setEstado(peticionChequeo.getEstado());
        dto.setFecha(peticionChequeo.getFecha());
        dto.setFechaUpdate(peticionChequeo.getFechaUpdate());
        dto.setNombreFichero(peticionChequeo.getNombreFichero());
        dto.setTarget(peticionChequeo.getTarget());
        dto.setPeridAutor(peticionChequeo.getPeridAutor());
        dto.setPeridProfesor(peticionChequeo.getPeridProfesor());
        dto.setEmailProfesor(peticionChequeo.getEmailProfesor());
        dto.setCursoAcademico(peticionChequeo.getCursoAcademico());
        dto.setAsignaturaId(peticionChequeo.getAsignaturaId());
        dto.setReferenciaFichero(peticionChequeo.getReferenciaFichero());
        dto.setScore(peticionChequeo.getSimilitud());

        return dto;
    }

    public static PeticionChequeoDTO toDTO (PeticionChequeo peticionChequeo) {
        PeticionChequeoDTO dto = new PeticionChequeoDTO();
        dto.setApellidosAutor(peticionChequeo.getApellidosAutor());
        dto.setId(peticionChequeo.getId());
        dto.setIdExterno(peticionChequeo.getIdExterno());
        dto.setNombreAutor(peticionChequeo.getNombreAutor());
        dto.setTitulo(peticionChequeo.getTitulo());
        dto.setUrlInforme(peticionChequeo.getUrlInforme());
        dto.setEstado(peticionChequeo.getEstado());
        dto.setFecha(peticionChequeo.getFecha());
        dto.setFechaUpdate(peticionChequeo.getFechaUpdate());
        dto.setNombreFichero(peticionChequeo.getNombreFichero());
        dto.setTarget(peticionChequeo.getTarget());
        dto.setPeridAutor(peticionChequeo.getPeridAutor());
        dto.setPeridProfesor(peticionChequeo.getPeridProfesor());
        dto.setEmailProfesor(peticionChequeo.getEmailProfesor());
        dto.setReferenciaFichero(peticionChequeo.getReferenciaFichero());
        dto.setScore(peticionChequeo.getSimilitud());

        return dto;
    }


    public PeticionChequeoDTO(){

    }

    public PeticionChequeoDTO(String json) {
        Gson g = new Gson();
        PeticionChequeoDTO dto = g.fromJson(json, PeticionChequeoDTO.class);
        this.id = dto.getId();
        this.estado = dto.getEstado();
        this.urlInforme = dto.getUrlInforme();
        this.nombreFichero = dto.getNombreFichero();
        this.nombreAutor = dto.getNombreAutor();
        this.apellidosAutor = dto.getApellidosAutor();
        this.titulo = dto.getTitulo();
        this.fecha = dto.getFecha();
        this.fechaUpdate = dto.getFechaUpdate();
        this.target = dto.getTarget();
        this.idExterno = dto.getIdExterno();
        this.peridAutor = dto.getPeridAutor();
        this.peridProfesor = dto.getPeridProfesor();
        this.emailProfesor = dto.getEmailProfesor();
        this.cursoAcademico = dto.getCursoAcademico();
        this.asignaturaId = dto.getAsignaturaId();
        this.referenciaFichero = dto.getReferenciaFichero();
        this.score = dto.getScore();
    }

    public String getIdExterno()
    {
        return idExterno;
    }

    public void setIdExterno(String idExterno)
    {
        this.idExterno = idExterno;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public String getUrlInforme()
    {
        return urlInforme;
    }

    public void setUrlInforme(String urlInforme)
    {
        this.urlInforme = urlInforme;
    }

    public String getNombreFichero()
    {
        return nombreFichero;
    }

    public void setNombreFichero(String nombreFichero)
    {
        this.nombreFichero = nombreFichero;
    }

    public String getNombreAutor()
    {
        return nombreAutor;
    }

    public void setNombreAutor(String nombreAutor)
    {
        this.nombreAutor = nombreAutor;
    }

    public String getApellidosAutor()
    {
        return apellidosAutor;
    }

    public void setApellidosAutor(String apellidosAutor)
    {
        this.apellidosAutor = apellidosAutor;
    }

    public String getTitulo()
    {
        return titulo;
    }

    public void setTitulo(String titulo)
    {
        this.titulo = titulo;
    }

    public Date getFecha()
    {
        return fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public Date getFechaUpdate()
    {
        return fechaUpdate;
    }

    public void setFechaUpdate(Date fechaUpdate)
    {
        this.fechaUpdate = fechaUpdate;
    }

    public String getTarget()
    {
        return target;
    }

    public void setTarget(String target)
    {
        this.target = target;
    }

    public Long getPeridAutor() {
        return peridAutor;
    }

    public void setPeridAutor(Long peridAutor) {
        this.peridAutor = peridAutor;
    }

    public Long getPeridProfesor() {
        return peridProfesor;
    }

    public void setPeridProfesor(Long peridProfesor) {
        this.peridProfesor = peridProfesor;
    }

    public Integer getCursoAcademico()
    {
        return cursoAcademico;
    }

    public void setCursoAcademico(Integer cursoAcademico)
    {
        this.cursoAcademico = cursoAcademico;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public String getReferenciaFichero() {
        return referenciaFichero;
    }

    public void setReferenciaFichero(String referenciaFichero) {
        this.referenciaFichero = referenciaFichero;
    }

    public String getEmailProfesor() {
        return emailProfesor;
    }

    public void setEmailProfesor(String emailProfesor) {
        this.emailProfesor = emailProfesor;
    }
    public Long getScore() {
        return score;
    }

    public void setScore(Long score) {
        this.score = score;
    }

}
