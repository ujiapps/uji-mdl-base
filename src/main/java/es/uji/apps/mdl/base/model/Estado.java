package es.uji.apps.mdl.base.model;

public enum Estado
{
    ADMINISTRATIVE("administrative"), OWNED_BY_TEACHERS("ownedbyteachers"), EDITED_BY_UNIVERSITY(
        "editedbyuniversity"), PUBLIC_DOMAIN("publicdomain"), LT10_FRAGMENT("lt10fragment"), HAVE_LICENSE(
        "havelicense"), NOT_PRINTABLE("notprintable"), OTHER("other");

    private String value;

    Estado(String value)
    {
        this.value = value;
    }

    public static boolean contains(String value)
    {
        for (Estado c : Estado.values())
        {
            if (c.name().equalsIgnoreCase(value))
            {
                return true;
            }
        }

        return false;
    }

    public String value()
    {
        return value;
    }
}
