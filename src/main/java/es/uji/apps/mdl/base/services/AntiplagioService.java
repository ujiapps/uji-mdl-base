package es.uji.apps.mdl.base.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.mdl.base.dao.AntiplagioDAO;
import es.uji.apps.mdl.base.dto.antiplagio.IdDocumentoDTO;
import es.uji.apps.mdl.base.dto.antiplagio.PeticionChequeoDTO;
import es.uji.apps.mdl.base.exceptions.PeticionNotFoundException;
import es.uji.apps.mdl.base.model.antiplagio.PeticionChequeo;

@Service
public class AntiplagioService
{
    @Autowired
    AntiplagioDAO antiplagioDAO;

    /**
     * Obtiene peticiones pendientes de chequeo.
     *
    * @return
     */
    public List<IdDocumentoDTO> obtenerPeticionesPendientesDeEnvio()
    {
        return antiplagioDAO.obtenerPeticionesPendientesDeChequeo()
                .stream()
                .map(IdDocumentoDTO::new)
                .collect(Collectors.toList());
    }

    /**
     * Actualiza una petición de chequeo. Sólo está permido cambiar:
     *
     *     El id externo.
     *     El estado
     *     La url del informe
     *
     * Sólo se actualizan si dichos campos no son nulos.
     *
     * @param peticionChequeoDTO
     * @return
     */
    public PeticionChequeoDTO actualizarPeticionChequeo (PeticionChequeoDTO peticionChequeoDTO) throws PeticionNotFoundException {

        assertPeticionExiste(peticionChequeoDTO.getId());

        PeticionChequeo peticionChequeo = antiplagioDAO.getPeticionChequeo(peticionChequeoDTO.getId());
        if (peticionChequeoDTO.getEstado() != null) {
            peticionChequeo.setEstado(peticionChequeoDTO.getEstado());
        }
        if (peticionChequeoDTO.getIdExterno() != null) {
            peticionChequeo.setIdExterno(peticionChequeoDTO.getIdExterno());
        }
        if (peticionChequeoDTO.getUrlInforme() != null) {
            peticionChequeo.setUrlInforme(peticionChequeoDTO.getUrlInforme());
        }
        if (peticionChequeoDTO.getScore() != null) {
            peticionChequeo.setSimilitud(peticionChequeoDTO.getScore());
        }

        antiplagioDAO.actualizarPeticionChequeo(peticionChequeo);

        peticionChequeo = antiplagioDAO.getPeticionChequeo(peticionChequeo.getId());
        return PeticionChequeoDTO.toDTO(peticionChequeo);
    }

    private void assertPeticionExiste(Long idPeticionChequeo) throws PeticionNotFoundException
    {
        if (!this.antiplagioDAO.existePeticionChequeo(idPeticionChequeo))
        {
            throw new PeticionNotFoundException(idPeticionChequeo);
        }
    }

    public List<IdDocumentoDTO> obtenerPeticionesEnviadas()
    {
        return this.antiplagioDAO.obtenerPeticionesEnviadas()
                .stream()
                .map(IdDocumentoDTO::new)
                .collect(Collectors.toList());
    }

    public PeticionChequeoDTO obtenerPeticion(Long idPeticionChequeo) throws PeticionNotFoundException
    {
        assertPeticionExiste(idPeticionChequeo);
        return PeticionChequeoDTO.toDTO(this.antiplagioDAO.getPeticionChequeo(idPeticionChequeo));
    }

    public PeticionChequeoDTO obtenerPeticionExtendida(Long idPeticionChequeo) throws PeticionNotFoundException
    {
        assertPeticionExiste(idPeticionChequeo);
        return PeticionChequeoDTO.toDTO(this.antiplagioDAO.getPeticionChequeoExtendida(idPeticionChequeo));
    }


    /**
     * Obtiene el contenido del fichero asociado a una petición.
     *
     * @param idPeticionChequeo
     * @return
     * @throws PeticionNotFoundException
     */
    public byte[] obtenerFichero(Long idPeticionChequeo) throws PeticionNotFoundException
    {
        assertPeticionExiste(idPeticionChequeo);
        return this.antiplagioDAO.getFichero(idPeticionChequeo);
    }

    /**
     * Dado el id de una petición de chequeo antiplagio de TFG, obtiene el listado de profesores que pueden ver
     * el informe de similitud. Básicamente, quien puede verlo es:
     *
     *    * Los profesores de la asignatura de grado.
     *    * Los coordinadores.
     *
     * @param idPeticionChequeo
     * @return
     * @throws PeticionNotFoundException
     */
    public List<Long> obtenerProfesores (Long idPeticionChequeo) throws PeticionNotFoundException
    {
        assertPeticionExiste(idPeticionChequeo);
        PeticionChequeo peticion = antiplagioDAO.getPeticionChequeo(idPeticionChequeo);
        return this.antiplagioDAO.obtenerProfesores(peticion.getSolicitudId());
    }
}
