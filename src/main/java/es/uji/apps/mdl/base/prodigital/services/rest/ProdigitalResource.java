package es.uji.apps.mdl.base.prodigital.services.rest;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.mdl.base.prodigital.dto.CursoFormativoDTO;
import es.uji.apps.mdl.base.prodigital.exceptions.ProdigitalException;
import es.uji.apps.mdl.base.prodigital.services.ProdigitalService;
import org.springframework.web.bind.annotation.RequestBody;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Este recurso se encuentra en su propio servlet, cuya URL es /rest/prodigital. Por este motivo, el Path
 * es /. La configuración en un servlet separado la hacemos para hacer una captura jersey más limpia de las excepciones
 * lanzadas por el recurso.
 */
@Path("/")
public class ProdigitalResource {

    @InjectParam
    private ProdigitalService prodigitalService;

    @POST
    @Path("/cursos")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response cursos(@RequestBody String jsonRequest) throws ProdigitalException {

        CursoFormativoDTO cursoFormativo;

        try {
            ObjectMapper objectMapper = new ObjectMapper();

            cursoFormativo = objectMapper.readValue(jsonRequest, CursoFormativoDTO.class);

            this.prodigitalService.validarCursoFormativo(cursoFormativo);
            this.prodigitalService.almacenarCursoFormativo(jsonRequest);

        } catch (ProdigitalException e) {
            throw e;
        } catch (UnrecognizedPropertyException e) {
            throw new ProdigitalException(
                    400,
                    e.getMessage(),
                    "unrecognizedproperty",
                    e
            );
        } catch (Exception e) {
            throw new ProdigitalException(
                    500,
                    e.getMessage(),
                    "unhandleddexception",
                    e
            );
        }

        return Response.ok().entity(cursoFormativo).build();
    }
}
