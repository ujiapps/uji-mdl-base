package es.uji.apps.mdl.base.model;

import java.util.ArrayList;
import java.util.List;

public class PeticionesPaginadasLPI
{
    private Integer total;
    private List<PeticionLPI> peticiones;

    public PeticionesPaginadasLPI()
    {
        peticiones = new ArrayList<>();
    }

    public Integer getTotal()
    {
        return total;
    }

    public void setTotal(Integer total)
    {
        this.total = total;
    }

    public List<PeticionLPI> getPeticiones()
    {
        return peticiones;
    }

    public void setPeticiones(List<PeticionLPI> peticiones)
    {
        this.peticiones = peticiones;
    }

    public void addPeticion(PeticionLPI peticionLPI)
    {
        this.peticiones.add(peticionLPI);
    }
}
