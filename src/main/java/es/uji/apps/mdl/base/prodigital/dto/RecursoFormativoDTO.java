package es.uji.apps.mdl.base.prodigital.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import es.uji.apps.mdl.base.prodigital.model.Nivel;
import es.uji.apps.mdl.base.prodigital.model.RecursoEducativo;

import java.util.Date;
import java.util.List;


@JsonIgnoreProperties(ignoreUnknown = true)
public class RecursoFormativoDTO {

    protected String id;

    protected List<String> autor;

    protected String titulo;

    protected String uriInterna;

    protected RecursoEducativo tipoRecursoEducativo;

    protected String idioma;

    protected List<String> palabrasClave;

    protected List<String> competenciasDigitales;

    protected String descripcion;

    protected Nivel nivel;

    protected String derechosAcceso;

    protected String derechosAccesoUri;

    protected String departamento;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    protected Date fechaPublicacion;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    protected Date fechaInclusion;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    protected Date fechaDisponibilidad;

    protected String uriExterna;

    protected List<AuditoriasDTO> auditorias;

    protected String tamanyoArchivo;

    protected String formatoArchivo;

    protected String estudios;

    protected String urlCurso;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getAutor() {
        return autor;
    }

    public void setAutor(List<String> autor) {
        this.autor = autor;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getUriInterna() {
        return uriInterna;
    }

    public void setUriInterna(String uriInterna) {
        this.uriInterna = uriInterna;
    }

    public RecursoEducativo getTipoRecursoEducativo() {
        return tipoRecursoEducativo;
    }

    public void setTipoRecursoEducativo(RecursoEducativo tipoRecursoEducativo) {
        this.tipoRecursoEducativo = tipoRecursoEducativo;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public List<String> getPalabrasClave() {
        return palabrasClave;
    }

    public void setPalabrasClave(List<String> palabrasClave) {
        this.palabrasClave = palabrasClave;
    }

    public List<String> getCompetenciasDigitales() {
        return competenciasDigitales;
    }

    public void setCompetenciasDigitales(List<String> competenciasDigitales) {
        this.competenciasDigitales = competenciasDigitales;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Nivel getNivel() {
        return nivel;
    }

    public void setNivel(Nivel nivel) {
        this.nivel = nivel;
    }

    public String getDerechosAcceso() {
        return derechosAcceso;
    }

    public void setDerechosAcceso(String derechosAcceso) {
        this.derechosAcceso = derechosAcceso;
    }

    public String getDerechosAccesoUri() {
        return derechosAccesoUri;
    }

    public void setDerechosAccesoUri(String derechosAccesoUri) {
        this.derechosAccesoUri = derechosAccesoUri;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public Date getFechaPublicacion() {
        return fechaPublicacion;
    }

    public void setFechaPublicacion(Date fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }

    public Date getFechaInclusion() {
        return fechaInclusion;
    }

    public void setFechaInclusion(Date fechaInclusion) {
        this.fechaInclusion = fechaInclusion;
    }

    public Date getFechaDisponibilidad() {
        return fechaDisponibilidad;
    }

    public void setFechaDisponibilidad(Date fechaDisponibilidad) {
        this.fechaDisponibilidad = fechaDisponibilidad;
    }

    public String getUriExterna() {
        return uriExterna;
    }

    public void setUriExterna(String uriExterna) {
        this.uriExterna = uriExterna;
    }

    public List<AuditoriasDTO> getAuditorias() {
        return auditorias;
    }

    public void setAuditorias(List<AuditoriasDTO> auditorias) {
        this.auditorias = auditorias;
    }

    public String getTamanyoArchivo() {
        return tamanyoArchivo;
    }

    public void setTamanyoArchivo(String tamanyoArchivo) {
        this.tamanyoArchivo = tamanyoArchivo;
    }

    public String getFormatoArchivo() {
        return formatoArchivo;
    }

    public void setFormatoArchivo(String formatoArchivo) {
        this.formatoArchivo = formatoArchivo;
    }

    public String getEstudios() {
        return estudios;
    }

    public void setEstudios(String estudios) {
        this.estudios = estudios;
    }

    public String getUrlCurso() {
        return urlCurso;
    }

    public void setUrlCurso(String urlCurso) {
        this.urlCurso = urlCurso;
    }
}
