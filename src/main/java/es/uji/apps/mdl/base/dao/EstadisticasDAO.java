package es.uji.apps.mdl.base.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.mdl.base.model.estadisticas.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class EstadisticasDAO extends BaseDAODatabaseImpl {

    QPodMoodle qPodMoodle = QPodMoodle.podMoodle;

    public List<Long> obtenerCursos (int cursoAcademico) {
        JPAQuery query = new JPAQuery(this.entityManager);
        return query
                .from(qPodMoodle)
                .where(
                        qPodMoodle.cursoAcademicoDeAlta.eq(cursoAcademico)
                ).list(qPodMoodle.id);
    }

    @Transactional
    public void borrarTablasCarga() {
        this.entityManager.createQuery("DELETE FROM MdlUser").executeUpdate();
        this.entityManager.createQuery("DELETE FROM MdlUserStudents").executeUpdate();
        this.entityManager.createQuery("DELETE FROM MdlUserTeachers").executeUpdate();
        this.entityManager.createQuery("DELETE FROM MdlCourse").executeUpdate();
    }

    @Transactional
    public void insertarUsers (List<MdlUser> models) {
        models.forEach(this::insert);
    }

    @Transactional
    public void insertarCourses(List<MdlCourse> models) { models.forEach(this::insert); }

    @Transactional
    public void insertarMdlUserStudents(List<MdlUserStudents> models) {
        models.forEach(this::insert);
    }

    @Transactional
    public void insertarMdlUserTeachers(List<MdlUserTeachers> models) {
        models.forEach(this::insert);
    }

    public void insertarMdlLogAccesos (MdlLogAccesos logaccesos) {
        this.insert(logaccesos);
    }

    public void insertarPodMoodleAccesos (PodMoodleAccesos model) {
        this.insert(model);
    }
}
