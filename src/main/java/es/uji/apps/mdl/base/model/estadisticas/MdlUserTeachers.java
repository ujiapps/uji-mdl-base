package es.uji.apps.mdl.base.model.estadisticas;

import javax.persistence.*;

@Entity
@Table(name = "gra_mdl.mdl_user_teachers")
public class MdlUserTeachers {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @Column
    String username;

    @Column(name = "ORA_COURSEID")
    Long oraCourseId;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getOraCourseId() {
        return oraCourseId;
    }

    public void setOraCourseId(Long oraCourseId) {
        this.oraCourseId = oraCourseId;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }
}
