package es.uji.apps.mdl.base.model.estadisticas;

import javax.persistence.*;

@Entity
@Table(name = "gra_mdl.mdl_course")
public class MdlCourse {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @Column(name = "fullname")
    private String fullname;

    @Column(name = "shortname")
    private String shortname;

    @Column(name = "procesar")
    private String procesar;

    @Column(name = "ora_courseid")
    private Long oraCourseId;

    @Column(name = "visible")
    private String visible;

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    public String getProcesar() {
        return procesar;
    }

    public void setProcesar(String procesar) {
        this.procesar = procesar;
    }

    public Long getOraCourseId() {
        return oraCourseId;
    }

    public void setOraCourseId(Long oraCourseId) {
        this.oraCourseId = oraCourseId;
    }

    public String getVisible() {
        return visible;
    }

    public void setVisible(String visible) {
        this.visible = visible;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }
}
