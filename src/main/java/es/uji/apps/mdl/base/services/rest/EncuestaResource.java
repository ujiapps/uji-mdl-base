package es.uji.apps.mdl.base.services.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.mdl.base.exceptions.UserNotFoundException;
import es.uji.apps.mdl.base.model.Encuesta;
import es.uji.apps.mdl.base.responses.RespuestaGeneral;
import es.uji.apps.mdl.base.services.EncuestaService;
import es.uji.commons.rest.ParamUtils;

@Path("encuestas")
public class EncuestaResource
{
    @InjectParam
    EncuestaService encuestaService;

    @GET
    @Path("{id}/pendientes")
    @Produces(MediaType.APPLICATION_JSON)
    public RespuestaGeneral<Encuesta> getEncuestasPendientes(@PathParam("id") Long perid) throws UserNotFoundException
    {
        ParamUtils.checkNotNull(perid);

        Encuesta e = this.encuestaService.getEncuentasPendientes(perid);
        if (e == null)
        {
            throw new UserNotFoundException(perid);
        }

        return RespuestaGeneral.toRespuesta(e);
    }
}

