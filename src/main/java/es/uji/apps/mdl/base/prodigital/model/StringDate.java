package es.uji.apps.mdl.base.prodigital.model;

import es.uji.apps.mdl.base.prodigital.exceptions.InvalidFormatException;

import java.util.regex.Pattern;

public class StringDate {

    protected static Pattern datePattern = Pattern.compile("^dd/mm/yyyy$");

    protected String fecha;

    StringDate (String fecha) throws InvalidFormatException {
        this.assertFormatoValido(fecha);
        this.fecha = fecha;
    }

    protected void assertFormatoValido(String fecha) throws InvalidFormatException {
        if (!datePattern.matcher(fecha).matches()) {
            throw new InvalidFormatException();
        }
    }
}
