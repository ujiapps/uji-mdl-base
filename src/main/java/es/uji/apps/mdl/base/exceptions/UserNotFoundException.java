package es.uji.apps.mdl.base.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Lanzada si el perid no se encontró en la vista
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "User does not exists")
public class UserNotFoundException extends Exception
{
    public UserNotFoundException(Long perid)
    {
        super("User with perid " + (perid != null ? perid.toString() : "null") + " not found");
    }
}
