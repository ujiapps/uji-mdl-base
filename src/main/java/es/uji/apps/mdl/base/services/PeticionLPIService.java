package es.uji.apps.mdl.base.services;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.mdl.base.dao.PeticionLPIDAO;
import es.uji.apps.mdl.base.exceptions.GettingFileException;
import es.uji.apps.mdl.base.exceptions.GettingPeticionLPIException;
import es.uji.apps.mdl.base.exceptions.GettingPeticionesLPIPendientesException;
import es.uji.apps.mdl.base.exceptions.SettingStatePeticionLPIException;
import es.uji.apps.mdl.base.model.FichaCompletaLPI;
import es.uji.apps.mdl.base.model.PeticionesPaginadasLPI;

@Service
public class PeticionLPIService
{
    @Autowired
    private PeticionLPIDAO peticionLPIDAO;

    public PeticionesPaginadasLPI getByQuery(Integer start, Integer limit, String query)
            throws GettingPeticionesLPIPendientesException
    {
        return peticionLPIDAO.getPeticionesLPI(start, limit, query);
    }

    public FichaCompletaLPI get(String peticionId)
            throws GettingPeticionLPIException
    {
        return peticionLPIDAO.getPeticionLPI(peticionId);
    }

    public InputStream getFileByUrl(String urlDescarga)
            throws IOException, GettingFileException
    {
        return peticionLPIDAO.getFile(urlDescarga);
    }

    public void aprobar(String peticionId, String estadoAprobacion, String fechaFinLicencia)
            throws SettingStatePeticionLPIException, ParseException
    {
        Long timestamp = 0L;

        if (fechaFinLicencia != null && fechaFinLicencia != "0")
        {
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date date = formatter.parse(fechaFinLicencia);
            timestamp = (long) date.getTime() / 1000;
        }

        peticionLPIDAO.aprobar(peticionId, estadoAprobacion, timestamp.toString());
    }

    public void denegar(String peticionId, String motivoDenegacion)
            throws SettingStatePeticionLPIException
    {
        peticionLPIDAO.denegar(peticionId, motivoDenegacion);
    }
}
