package es.uji.apps.mdl.base.model.borrado;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "gra_pod.pod_moodle_borrado")
public class PeticionBorrado
{
    /**
     * No vamos a hacer inserción de peticiones de borrado desde el servicio, por lo que podemos prescindir de definir
     * id como un GeneratedValue
     */
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "pme_id")
    private Long pmeId;

    @Column(name = "fecha_solicitud")
    private LocalDateTime fechaSolicitud;

    @Column(name = "estado")
    private String estado;

    @Column(name = "solicitante_id")
    private Long solicitanteId;

    @OneToMany(mappedBy = "peticionBorrado")
    private List<PeticionBorradoLog> logs;

    @Column(name = "info")
    private String info;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getPmeId()
    {
        return pmeId;
    }

    public void setPmeId(Long pmeId)
    {
        this.pmeId = pmeId;
    }

    public LocalDateTime getFechaSolicitud()
    {
        return fechaSolicitud;
    }

    public void setFechaSolicitud(LocalDateTime fechaSolicitud)
    {
        this.fechaSolicitud = fechaSolicitud;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public Long getSolicitanteId()
    {
        return solicitanteId;
    }

    public void setSolicitanteId(Long solicitanteId)
    {
        this.solicitanteId = solicitanteId;
    }

    public List<PeticionBorradoLog> getLogs()
    {
        return logs;
    }

    public void setLogs(List<PeticionBorradoLog> logs)
    {
        this.logs = logs;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
