package es.uji.apps.mdl.base.model.estadisticas;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "gra_pod.pod_moodle")
public class PodMoodle {

    @Id
    @Column
    Long id;

    @Column(name="curso_aca_alta")
    Integer cursoAcademicoDeAlta;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCursoAcademicoDeAlta() {
        return cursoAcademicoDeAlta;
    }

    public void setCursoAcademicoDeAlta(Integer cursoAcademicoInicial) {
        this.cursoAcademicoDeAlta = cursoAcademicoInicial;
    }
}
