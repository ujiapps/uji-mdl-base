package es.uji.apps.mdl.base.prodigital.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CursoFormativoDTO {

    protected Long id;

    protected String usuario;

    protected String universidad;

    protected String curso;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    protected Date fechaRealizacion;

    @JsonProperty(value = "RecursoFormativoDTO", required = true)
    protected RecursoFormativoDTO recursoFormativoDTO;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getUniversidad() {
        return universidad;
    }

    public void setUniversidad(String universidad) {
        this.universidad = universidad;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public Date getFechaRealizacion() {
        return fechaRealizacion;
    }

    public void setFechaRealizacion(Date fechaRealizacion) {
        this.fechaRealizacion = fechaRealizacion;
    }

    public es.uji.apps.mdl.base.prodigital.dto.RecursoFormativoDTO getRecursoFormativoDTO() {
        return recursoFormativoDTO;
    }

    public void setRecursoFormativoDTO(es.uji.apps.mdl.base.prodigital.dto.RecursoFormativoDTO recursoFormativoDTO) {
        this.recursoFormativoDTO = recursoFormativoDTO;
    }
}
