package es.uji.apps.mdl.base.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

public class GettingFileException extends CoreDataBaseException
{
    public GettingFileException()
    {
        super("Errada recuperant un fitxer");
    }

    public GettingFileException(String message)
    {
        super(message);
    }
}
