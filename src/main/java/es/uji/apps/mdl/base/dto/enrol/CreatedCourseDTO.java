package es.uji.apps.mdl.base.dto.enrol;

public class CreatedCourseDTO {
    Long courseid;
    Long courseidnumber;

    public Long getCourseid() {
        return courseid;
    }

    public void setCourseid(Long courseid) {
        this.courseid = courseid;
    }

    public Long getCourseidnumber() {
        return courseidnumber;
    }

    public void setCourseidnumber(Long courseidnumber) {
        this.courseidnumber = courseidnumber;
    }
}
