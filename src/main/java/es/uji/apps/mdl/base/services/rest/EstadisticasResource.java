package es.uji.apps.mdl.base.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.mdl.base.dto.estadisticas.*;
import es.uji.apps.mdl.base.services.EstadisticasService;
import org.springframework.web.bind.annotation.RequestBody;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("estadisticas")
public class EstadisticasResource {

    @InjectParam
    private EstadisticasService estadisticasService;

    @DELETE
    public void borrarTablasCarga() {
        this.estadisticasService.borrarTablasDeCarga();
    }

    @POST
    @Path("/mdl_user")
    @Consumes(MediaType.APPLICATION_JSON)
    public void insertarUsers (@RequestBody List<MdlUserDTO> user) {
        this.estadisticasService.insertarUsers(user);
    }

    @POST
    @Path("/mdl_user_teachers")
    @Consumes(MediaType.APPLICATION_JSON)
    public void insertarTeachers (@RequestBody List<MdlUserTeachersDTO> userTeachers) {
        this.estadisticasService.insertarUserTeachers(userTeachers);
    }

    @POST
    @Path("/mdl_user_students")
    @Consumes(MediaType.APPLICATION_JSON)
    public void insertarStudents (@RequestBody List<MdlUserStudentsDTO> users) {
        this.estadisticasService.insertarUserStudents(users);
    }

    @POST
    @Path("/mdl_course")
    @Consumes(MediaType.APPLICATION_JSON)
    public void insertarCourses (@RequestBody List<MdlCourseDTO> courses) {
        this.estadisticasService.insertarCourses(courses);
    }

    @POST
    @Path("/accesos")
    @Consumes(MediaType.APPLICATION_JSON)
    public void insertarCourses (@RequestBody AccesosDTO courses) {
        this.estadisticasService.insertarAccesos(courses);
    }

    @GET
    @Path("/cursos/{curso_academico}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Long> obtenerCursos (@PathParam("curso_academico") int cursoAcademico) {
        return this.estadisticasService.obtenerCursos(cursoAcademico);
    }
}
