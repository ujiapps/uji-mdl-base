package es.uji.apps.mdl.base.dao;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.mdl.base.model.Encuesta;
import es.uji.apps.mdl.base.model.QEncuesta;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class EncuestaDAO extends BaseDAODatabaseImpl
{
    private QEncuesta qEncuesta = QEncuesta.encuesta;

    public Encuesta getEncuestasPendientes (Long perid) {
        JPAQuery query = new JPAQuery(this.entityManager);
        return query.from(qEncuesta).where(qEncuesta.perid.eq(perid)).uniqueResult(qEncuesta);
    }
}
