package es.uji.apps.mdl.base.model.enrol;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "gra_pod.pod_moodle")
public class Course
{
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "sincronizado")
    private String sincronizado;

    @Column(name = "idmoodle")
    private Long idmoodle;


    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getSincronizado()
    {
        return sincronizado;
    }

    public void setSincronizado(String sincronizado)
    {
        this.sincronizado = sincronizado;
    }

    public Long getIdmoodle() {
        return idmoodle;
    }

    public void setIdmoodle(Long idmoodle) {
        this.idmoodle = idmoodle;
    }
}
