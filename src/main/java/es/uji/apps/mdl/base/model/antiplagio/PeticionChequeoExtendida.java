package es.uji.apps.mdl.base.model.antiplagio;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import static javax.persistence.FetchType.LAZY;

/**
 * Contiene información adicional extraída a partir del id de SPI de la solicitud.
 */

@Entity
@Table(name = "antiplagio_v_peticiones_tfg")
public class PeticionChequeoExtendida
{
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "estado")
    private String estado;

    @Column(name = "url_informe")
    private String urlInforme;

    @Column(name = "nombre_fichero")
    private String nombreFichero;

    @Column(name = "nombre_autor")
    private String nombreAutor;

    @Column(name = "apellidos_autor")
    private String apellidosAutor;

    @Column(name = "titulo")
    private String titulo;

    @Lob
    @Basic(fetch = LAZY)
    @Column(name = "fichero", columnDefinition = "BLOB")
    private byte[] fichero;

    @Column(name = "fecha")
    private Date fecha;

    @Column(name = "fecha_update")
    private Date fechaUpdate;

    @Column(name = "target")
    private String target;

    @Column(name = "id_externo")
    private String idExterno;

    @Column(name = "intentos")
    private Integer intentos;

    @Column(name = "perid_autor")
    private Long peridAutor;

    @Column(name = "perid_profesor")
    private Long peridProfesor;

    @Column(name = "email_profesor")
    private String emailProfesor;

    @Column(name = "sol_id")
    private Long solicitudId;

    @Column(name = "asi_id")
    private String asignaturaId;

    @Column(name = "curso_aca")
    private Integer cursoAcademico;

    @Column(name = "referencia_fichero")
    private String referenciaFichero;

    @Column(name = "similitud")
    private Long similitud;

    public Long getSimilitud() {
        return similitud;
    }

    public void setSimilitud(Long similitud) {
        this.similitud = similitud;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public Integer getCursoAcademico()
    {
        return cursoAcademico;
    }

    public void setCursoAcademico(Integer cursoAcademico)
    {
        this.cursoAcademico = cursoAcademico;
    }

    public Integer getIntentos()
    {
        return intentos;
    }

    public void setIntentos(Integer intentos)
    {
        this.intentos = intentos;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public String getUrlInforme()
    {
        return urlInforme;
    }

    public void setUrlInforme(String urlInforme)
    {
        this.urlInforme = urlInforme;
    }

    public String getNombreFichero()
    {
        return nombreFichero;
    }

    public void setNombreFichero(String nombreFichero)
    {
        this.nombreFichero = nombreFichero;
    }

    public String getNombreAutor()
    {
        return nombreAutor;
    }

    public void setNombreAutor(String nombreAutor)
    {
        this.nombreAutor = nombreAutor;
    }

    public String getApellidosAutor()
    {
        return apellidosAutor;
    }

    public void setApellidosAutor(String apellidosAutor)
    {
        this.apellidosAutor = apellidosAutor;
    }

    public String getTitulo()
    {
        return titulo;
    }

    public void setTitulo(String titulo)
    {
        this.titulo = titulo;
    }

    public byte[] getFichero()
    {
        return fichero;
    }

    public void setFichero(byte[] fichero)
    {
        this.fichero = fichero;
    }

    public Date getFecha()
    {
        return fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public Date getFechaUpdate()
    {
        return fechaUpdate;
    }

    public void setFechaUpdate(Date fechaUpdate)
    {
        this.fechaUpdate = fechaUpdate;
    }

    public String getTarget()
    {
        return target;
    }

    public void setTarget(String target)
    {
        this.target = target;
    }

    public String getIdExterno()
    {
        return idExterno;
    }

    public void setIdExterno(String idExterno)
    {
        this.idExterno = idExterno;
    }

    public Long getPeridAutor() {
        return peridAutor;
    }

    public void setPeridAutor(Long peridAutor) {
        this.peridAutor = peridAutor;
    }

    public Long getPeridProfesor() {
        return peridProfesor;
    }

    public void setPeridProfesor(Long peridProfesor) {
        this.peridProfesor = peridProfesor;
    }

    public String getEmailProfesor() {
        return emailProfesor;
    }

    public void setEmailProfesor(String emailProfesor) {
        this.emailProfesor = emailProfesor;
    }

    public Long getSolicitudId()
    {
        return solicitudId;
    }

    public void setSolicitudId(Long solicitudId)
    {
        this.solicitudId = solicitudId;
    }

    public String getReferenciaFichero() {
        return referenciaFichero;
    }

    public void setReferenciaFichero(String referenciaFichero) {
        this.referenciaFichero = referenciaFichero;
    }
}
