package es.uji.apps.mdl.base.prodigital.dto;

import es.uji.apps.mdl.base.prodigital.exceptions.ProdigitalException;
import org.codehaus.jackson.map.exc.UnrecognizedPropertyException;

public class ProdigitalExceptionDTO {

    protected boolean error;
    protected String codigoHttp;
    protected String descripcionError;
    protected String codigoError;

    public ProdigitalExceptionDTO(ProdigitalException e) {
        this.error = true;
        this.codigoHttp = Integer.toString(e.getCodigoHttp());
        this.descripcionError = e.getDescripcionError();
        this.codigoError = e.getCodigoError();
    }

    public ProdigitalExceptionDTO(Exception e) {
        this.error = true;
        this.codigoHttp = "500";
        this.descripcionError = e.getMessage();
        this.codigoError = "generalerror";
    }

    public ProdigitalExceptionDTO(UnrecognizedPropertyException e) {
        this.error = true;
        this.codigoError = "400";
        this.descripcionError = "Campo no reconocido en petición: " + e.getUnrecognizedPropertyName();
        this.codigoError = "unrecognizedpropertyerror";
    }

    public boolean isError() {
        return error;
    }

    public String getCodigoHttp() {
        return codigoHttp;
    }

    public String getDescripcionError() {
        return descripcionError;
    }

    public String getCodigoError() {
        return codigoError;
    }

}
