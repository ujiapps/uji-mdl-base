package es.uji.apps.mdl.base.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

public class SettingStatePeticionLPIException extends CoreDataBaseException
{
    public SettingStatePeticionLPIException()
    {
        super("Errada canviant l'estat d'una petició");
    }

    public SettingStatePeticionLPIException(String message)
    {
        super(message);
    }
}
