package es.uji.apps.mdl.base.model.antiplagio;

public enum EstadoChequeoAntiplagio
{
    PENDIENTE("PENDIENTE"),
    PREENVIADO("PREENVIADO"),
    ENVIADO("ENVIADO"),
    FINALIZADO("FINALIZADO"),
    ERROR("ERROR");

    private final String estado;

    EstadoChequeoAntiplagio(String estado)
    {
        this.estado = estado;
    }

    @Override
    public String toString()
    {
        return this.estado;
    }
}
