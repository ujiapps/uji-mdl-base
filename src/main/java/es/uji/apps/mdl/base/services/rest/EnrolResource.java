package es.uji.apps.mdl.base.services.rest;

import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.mdl.base.dto.enrol.CreatedCourseDTO;
import es.uji.apps.mdl.base.services.EnrolService;
import org.springframework.web.bind.annotation.RequestBody;

@Path("enrol")
public class EnrolResource
{
    @InjectParam
    EnrolService enrolService;

    @PUT
    @Path("course/{courseid}/markAsCreated")
    public void markAsCreated(@PathParam("courseid") Long courseId)
    {
        enrolService.markCourseAsCreated(courseId);
    }

    @PUT
    @Path("course/markAsCreated/{courseid}")
    public void markAsCreated(@PathParam("courseid") Long courseId, @RequestBody CreatedCourseDTO createdCourse) {
        enrolService.markCourseAsCreated(createdCourse);
    }

}
