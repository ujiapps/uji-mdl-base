package es.uji.apps.mdl.base.prodigital.services;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.uji.apps.mdl.base.prodigital.dao.ProdigitalDAO;
import es.uji.apps.mdl.base.prodigital.dto.CursoFormativoDTO;
import es.uji.apps.mdl.base.prodigital.exceptions.MissingRequiredFieldException;
import es.uji.apps.mdl.base.prodigital.exceptions.ValueNotAllowedException;
import es.uji.apps.mdl.base.prodigital.model.CursoFormativo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;

@Service
public class ProdigitalService {

    @Autowired
    ProdigitalDAO prodigitalDAO;

    public CursoFormativoDTO almacenarCursoFormativo (String cursoFormativoJSON) throws IOException {
        CursoFormativo cursoFormativo = new CursoFormativo();
        cursoFormativo.setCursoFormativo(cursoFormativoJSON);

        this.prodigitalDAO.almacenarCursoFormativo(cursoFormativo);

        cursoFormativo = this.prodigitalDAO.getCursoFormativoById(cursoFormativo.getId());
        return this.getCursoFormativoDTO(cursoFormativo);
    }

    private CursoFormativoDTO getCursoFormativoDTO (CursoFormativo cursoFormativo) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper
                .setSerializationInclusion(JsonInclude.Include.NON_NULL)
                ;
        return objectMapper.readValue(cursoFormativo.getCursoFormativo(), CursoFormativoDTO.class);
    }

    public void validarCursoFormativo (CursoFormativoDTO cursoFormativo) throws ValueNotAllowedException, MissingRequiredFieldException {
        this.validarCamposObligatorios(cursoFormativo);
        this.validarUniversidad(cursoFormativo);
        this.validarFechaRealizacion(cursoFormativo);
    }

    private void validarCamposObligatorios (CursoFormativoDTO cursoFormativo) throws MissingRequiredFieldException {
        String missingParam = null;
        if (cursoFormativo.getUsuario() == null || cursoFormativo.getUsuario().isEmpty()) {
            missingParam = "usuario";
        } else if (cursoFormativo.getUniversidad() == null || cursoFormativo.getUniversidad().isEmpty()) {
            missingParam = "universidad";
        } else if (cursoFormativo.getFechaRealizacion() == null) {
            missingParam = "fechaRealizacion";
        } else if (cursoFormativo.getCurso() == null || cursoFormativo.getCurso().isEmpty()) {
            missingParam = "cursoFormativo";
        } else if (cursoFormativo.getRecursoFormativoDTO() == null) {
            missingParam = "RecursoFormativoDTO";
        } else if (cursoFormativo.getRecursoFormativoDTO().getCompetenciasDigitales() == null || cursoFormativo.getRecursoFormativoDTO().getCompetenciasDigitales().isEmpty()) {
            missingParam = "competenciasDigitales";
        } else if (cursoFormativo.getRecursoFormativoDTO().getDescripcion() == null || cursoFormativo.getRecursoFormativoDTO().getDescripcion().isEmpty()) {
            missingParam = "descripcion";
        } else if (cursoFormativo.getRecursoFormativoDTO().getNivel() == null) {
            missingParam = "nivel";
        } else if (cursoFormativo.getRecursoFormativoDTO().getTitulo() == null || cursoFormativo.getRecursoFormativoDTO().getTitulo().isEmpty()) {
            missingParam = "titulo";
        }

        if (missingParam != null) throw new MissingRequiredFieldException(missingParam);
    }

    private void validarUniversidad (CursoFormativoDTO cursoFormativo) throws ValueNotAllowedException {
        if (!Arrays.asList("UJI", "UA", "UMH", "UV", "UPV").contains(cursoFormativo.getUniversidad())) {
            throw new ValueNotAllowedException("universidad", cursoFormativo.getUniversidad());
        }
    }

    private void validarFechaRealizacion (CursoFormativoDTO cursoFormativo) throws ValueNotAllowedException {
        if (cursoFormativo.getFechaRealizacion().after(new Date())) {
            throw new ValueNotAllowedException("fechaRealizacion", cursoFormativo.getFechaRealizacion().toString());
        }
    }
}
