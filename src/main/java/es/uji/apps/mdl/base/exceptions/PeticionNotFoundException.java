package es.uji.apps.mdl.base.exceptions;

public class PeticionNotFoundException extends Exception
{
    private Long peticionId;

    public PeticionNotFoundException(Long peticionId)
    {
        super(String.format("La petición con id %d no existe", peticionId));
    }
}
