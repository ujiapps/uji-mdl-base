package es.uji.apps.mdl.base.prodigital.model;

public enum RecursoEducativo {
        CURSO,
        VIDEO,
        PRESENTACION,
        DOCUMENTO_DE_TEXTO,
        HOJA_DE_CALCULO,
        IMAGEN_O_GRAFICO,
        OTROS;
}
