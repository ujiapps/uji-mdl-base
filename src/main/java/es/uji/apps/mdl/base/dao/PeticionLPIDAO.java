package es.uji.apps.mdl.base.dao;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.JsonNode;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;

import es.uji.apps.mdl.base.exceptions.GettingFileException;
import es.uji.apps.mdl.base.exceptions.GettingPeticionLPIException;
import es.uji.apps.mdl.base.exceptions.GettingPeticionesLPIPendientesException;
import es.uji.apps.mdl.base.exceptions.SettingStatePeticionLPIException;
import es.uji.apps.mdl.base.model.FichaCompletaLPI;
import es.uji.apps.mdl.base.model.PeticionLPI;
import es.uji.apps.mdl.base.model.PeticionesPaginadasLPI;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class PeticionLPIDAO extends BaseDAODatabaseImpl
{
    @Value("${uji.external.endPoint}")
    private String endPoint;

    @Value("${uji.external.authToken}")
    private String token;

    public void aprobar(String peticionId, String estado, String timestamp)
            throws SettingStatePeticionLPIException
    {
        setState(peticionId, "ok", estado, timestamp, "");
    }

    private void setState(String peticionId, String estadoAprobacion, String estado, String timestamp,
                          String motivoDenegacion)
            throws SettingStatePeticionLPIException
    {
        WebResource setState = Client.create()
                .resource(endPoint)
                .queryParam("wsfunction", "local_lpi_set_file_authorization")
                .queryParam("moodlewsrestformat", "json")
                .queryParam("id", peticionId.toString())
                .queryParam("authz", estadoAprobacion)
                .queryParam("documentcontent", estado)
                .queryParam("timeendlicense", timestamp)
                .queryParam("denyreason", motivoDenegacion);

        ClientResponse response = setState.type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class, getPostTokenParameter("wstoken"));

        if (response.getStatus() != 200)
        {
            throw new SettingStatePeticionLPIException();
        }

        JsonNode entity = response.getEntity(JsonNode.class);

        if (entity.get("errorcode") != null)
        {
            throw new SettingStatePeticionLPIException("Error:" + entity.get("message").asText());
        }
    }

    private MultivaluedMap getPostTokenParameter(String nameParameter)
    {
        MultivaluedMap postData = new MultivaluedMapImpl();

        postData.add(nameParameter, token);

        return postData;
    }

    public void denegar(String peticionId, String motivoDenegacion)
            throws SettingStatePeticionLPIException
    {
        setState(peticionId, "forbidden", "", "0", motivoDenegacion);
    }

    public InputStream getFile(String url)
            throws GettingFileException, IOException
    {
        WebResource getFile = Client.create().resource(url);

        ClientResponse response = getFile.type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class, getPostTokenParameter("token"));

        if (response.getStatus() != 200)
        {
            throw new GettingFileException();
        }

        return response.getEntityInputStream();
    }

    public FichaCompletaLPI getPeticionLPI(String peticionId)
            throws GettingPeticionLPIException
    {
        WebResource getPeticiones = Client.create()
                .resource(endPoint)
                .queryParam("wsfunction", "local_lpi_get_file_metadata")
                .queryParam("moodlewsrestformat", "json")
                .queryParam("id", peticionId.toString());

        ClientResponse response = getPeticiones.type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class, getPostTokenParameter("wstoken"));

        if (response.getStatus() != 200)
        {
            throw new GettingPeticionLPIException();
        }

        JsonNode entity = response.getEntity(JsonNode.class);

        if (entity.get("errorcode") != null)
        {
            throw new GettingPeticionLPIException("Error:" + entity.get("message").asText());
        }

        return entityToFichaCompletaPeticion(entity);
    }

    private FichaCompletaLPI entityToFichaCompletaPeticion(JsonNode entity)
    {
        FichaCompletaLPI peticion = new FichaCompletaLPI();

        peticion.setId(getText(entity.get("id")));
        peticion.setTitulo(getText(entity.get("title")));
        peticion.setAutor(getText(entity.get("author")));
        peticion.setEditorial(getText(entity.get("publisher")));

        peticion.setIdentificacion(getText(entity.get("identification")));
        peticion.setUrl(getText(entity.get("url")));
        peticion.setTotalMatriculados(getInteger(entity.get("enroled")));
        peticion.setPaginas(getText(entity.get("pages")));
        peticion.setTotalPaginas(getInteger(entity.get("totalpages")));

        peticion.setUsuarioRellenaMetadatos(getText(entity.get("metadatauseremail")));
        peticion.setUsuarioSubidaFichero(getText(entity.get("uploaduseremail")));
        peticion.setFechaRellenado(getDate(entity.get("metadatatimecreated")));
        peticion.setFechaSubida(getDate(entity.get("uploadtimecreated")));

        peticion.setUrlDescarga(getText(entity.get("url")));
        peticion.setContentTypeFile(getText(entity.get("mimetype")));
        peticion.setNameFile(getText(entity.get("filename")));
        peticion.setSizeFile(getLong(entity.get("filesize")));
        peticion.setEstado(getText(entity.get("state")));

        peticion.setComentarios(getText(entity.get("comments")));

        return peticion;
    }

    private String getText(JsonNode node)
    {
        if (node == null)
        {
            return null;
        }

        String value = node.asText();

        if (value == null || value.isEmpty() || value.equals("null"))
        {
            return null;
        }

        return value;
    }

    private Integer getInteger(JsonNode node)
    {
        if (node != null)
        {
            return node.asInt();
        }

        return null;
    }

    private Long getLong(JsonNode node)
    {
        if (node != null)
        {
            return node.asLong();
        }

        return null;
    }

    private Date getDate(JsonNode node)
    {
        if (node == null)
        {
            return null;
        }

        Long millis = node.asLong() * 1000;

        return new Date(millis);
    }

    public PeticionesPaginadasLPI getPeticionesLPI(Integer start, Integer limit, String query)
            throws GettingPeticionesLPIPendientesException
    {
        if (query == null)
        {
            query = "";
        }

        WebResource getPeticiones = Client.create()
                .resource(endPoint)
                .queryParam("wsfunction", "local_lpi_get_pending_files")
                .queryParam("moodlewsrestformat", "json")
                .queryParam("page", String.valueOf(start / limit))
                .queryParam("count", limit.toString())
                .queryParam("search", query);

        ClientResponse response = getPeticiones.type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class, getPostTokenParameter("wstoken"));

        if (response.getStatus() != 200)
        {
            throw new GettingPeticionesLPIPendientesException();
        }

        JsonNode root = response.getEntity(JsonNode.class);

        if (root.get("errorcode") != null)
        {
            throw new GettingPeticionesLPIPendientesException("Error:" + root.get("message").asText());
        }

        PeticionesPaginadasLPI peticiones = new PeticionesPaginadasLPI();

        peticiones.setTotal(root.get("count").asInt());

        for (JsonNode entity : root.get("data"))
        {
            peticiones.addPeticion(entityToPeticion(entity));
        }

        return peticiones;
    }

    private PeticionLPI entityToPeticion(JsonNode entity)
    {
        PeticionLPI peticion = new PeticionLPI();

        peticion.setId(getText(entity.get("id")));
        peticion.setTitulo(getText(entity.get("title")));
        peticion.setAutor(getText(entity.get("author")));
        peticion.setTipo(getText(entity.get("state")));
        peticion.setUsuario(getText(entity.get("metadatauseremail")));
        peticion.setFecha(getDate(entity.get("metadatatimecreated")));
        peticion.setEstado("Pendent");

        return peticion;
    }

    private Boolean getBoolean(JsonNode node)
    {
        if (node != null)
        {
            return node.asBoolean();
        }

        return null;
    }
}
