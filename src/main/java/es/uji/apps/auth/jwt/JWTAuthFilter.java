package es.uji.apps.auth.jwt;

import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jwt.SignedJWT;
import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Component
public class JWTAuthFilter extends OncePerRequestFilter {

    protected byte[] key;

    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        String header = httpServletRequest.getHeader("Authorization");
        if(header == null || !header.startsWith("Bearer ")) {
            httpServletResponse.setStatus(401);
            httpServletResponse.setHeader("WWW-Authenticate", "Bearer realm=\"prodigital\"");
            return;
        }

        // 3. Get the token
        String token = header.replace("Bearer ", "");

        try {
            SignedJWT jwt = SignedJWT.parse(token);
            JWSVerifier verifier = new MACVerifier(this.key);
            if (!jwt.verify(verifier)) {
                httpServletResponse.setStatus(401);
                httpServletResponse.setHeader("WWW-Authenticate", "Bearer realm=\"prodigital\"");
                return;
            }
        } catch (Exception e) {
            e.printStackTrace(System.err);
            httpServletResponse.setStatus(401);
            httpServletResponse.setHeader("WWW-Authenticate", "Bearer realm=\"prodigital\"");
            return;
        }

        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    public byte[] getKey() {
        return this.key;
    }

    public void setKey(String base64Key) {
        this.key = Base64.decodeBase64(base64Key.getBytes());
    }
}

