var encodeBase64 = function(str)
{
    if (typeof btoa === 'function')
    {
        return btoa(str);
    }
    var base64s = this.base64s;
    var bits;
    var dual;
    var i = 0;
    var encOut = "";
    while (str.length >= i + 3)
    {
        bits = (str.charCodeAt(i++) & 0xff) << 16 | (str.charCodeAt(i++) & 0xff) << 8 | str.charCodeAt(i++) & 0xff;
        encOut += base64s.charAt((bits & 0x00fc0000) >> 18) + base64s.charAt((bits & 0x0003f000) >> 12) + base64s.charAt((bits & 0x00000fc0) >> 6) + base64s.charAt((bits & 0x0000003f));
    }
    if (str.length - i > 0 && str.length - i < 3)
    {
        dual = Boolean(str.length - i - 1);
        bits = ((str.charCodeAt(i++) & 0xff) << 16) | (dual ? (str.charCodeAt(i) & 0xff) << 8 : 0);
        encOut += base64s.charAt((bits & 0x00fc0000) >> 18) + base64s.charAt((bits & 0x0003f000) >> 12) + (dual ? base64s.charAt((bits & 0x00000fc0) >> 6) : '=') + '=';
    }
    return (encOut);
};

Ext.define('UJI.grid.CSVExportPlugin',
{
    extend : 'Ext.plugin.Abstract',
    alias : 'plugin.csvExportPlugin',

    init : function(grid)
    {
        this.callParent([ grid ]);
        this.setCmp(grid);

        var tbar = grid.down('toolbar[dock=top]') || grid.addDocked({xtype : 'toolbar', dock : 'top' })[0];
        var button = new Ext.Button({ text : 'Export CSV' });
        tbar.add(button);

        button.addListener('click', this.exportCSV.bind(grid));
    },

    exportCSV : function(separator, includeHidden)
    {
        separator = separator || ';';
        includeHidden = includeHidden || true;

        var text = "";
        var columns = this.columnManager.columns;
        var columnsCount = columns.length;
        for (var i = 0; i < columnsCount; i++)
        {
            if (includeHidden || !columns[i].hidden)
            {
                text += columns[i].text + separator;
            }
        }
        text = text.substring(0, text.length - 1);
        text += "\r";

        var rows = this.store.data.items;
        var rowsCount = rows.length;
        for (var i = 0; i < rowsCount; i++)
        {
            var row = rows[i].data;
            for (var j = 0; j < columnsCount; j++)
            {
                if (includeHidden || !columns[j].hidden)
                {
                    var value = row[columns[j].dataIndex];
                    text += "\"" + value + "\"" + separator;
                }
            }
            text = text.substring(0, text.length - 1);
            text += "\r";
        }
        window.open('data:text/csv;charset=utf-8;base64,' + encodeBase64(text));
    }
});