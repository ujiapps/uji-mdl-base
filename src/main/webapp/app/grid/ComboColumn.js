Ext.define('UJI.grid.ComboColumn', {
    extend: 'Ext.grid.column.Column',
    alias: 'widget.ujicombocolumn',

    config: {
        header: '',
        dataIndex: '',
        resource: '',
        combo: {}
    },

    initComponent: function () {
        if (!this.combo.xtype) {
            this.combo = Ext.create({
                xtype: 'combobox',
                store: Ext.create('UJI.data.Store', {
                    resource: this.resource
                }),
                displayField: 'nombre',
                valueField: 'id',
                allowBlank: false,
                triggerAction: 'all',
                mode: 'local',
                editable: false
            });
        }

        this.editor = this.combo;
        this.renderer = this.comboRenderer(this.combo);

        this.callParent(arguments);
    },

    comboRenderer: function (combo) {
        return function (value) {
            var record = combo.store.findRecord(combo.valueField, value, 0, false, false, true);
            return record ? record.get(combo.displayField) : combo.valueNotFoundText;
        };
    }
});
