Ext.define('UJI.grid.PanelController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.ujigridpanelcontroller',

    onLoad: function () {
        var grid = this.getView();
        grid.getStore().load();
    },

    onSelect: function () {
        var grid = this.getView();
        grid.disableButtons(false);
    },

    onDeselect: function () {
        var grid = this.getView();
        grid.disableButtons(true);
    },

    onAdd: function () {
        var grid = this.getView();

        if (!grid.allowEdit) return;
        var rec = Ext.create(grid.getStore().model.entityName);
        var editor = grid.plugins[0];
        editor.cancelEdit();
        grid.getStore().insert(0, rec);
        editor.startEdit(rec, 0);
    },

    onEdit: function () {
        var grid = this.getView();
        if (!grid.allowEdit) return;

        var selection = grid.getSelectedRow();

        if (selection) {
            var editor = grid.plugins[0];
            editor.cancelEdit();
            editor.startEdit(selection);
        }
    },

    onDelete: function () {
        var grid = this.getView();
        var records = grid.getSelection();

        if (records.length === 1 && records[0].phantom === true) {
            return grid.getStore().remove(records);
        }

        if (records.length > 0) {
            Ext.Msg.confirm('Esborrar', 'Segur que voleu esborrar el registre?', function (btn, text) {
                if (btn == 'yes') {
                    grid.getStore().remove(records);
                    grid.disableButtons(true);
                    grid.updateElementCount();
                 //   grid.getStore().sync();
                }
            });
        }
    },

    onCancelEdit: function (editor, context) {
        var grid = this.getView();
        var record = context.record;

        if (record.phantom) {
            grid.getStore().remove(context.record);
            grid.getStore().sync();
        }
    },

    onReload: function () {
        var grid = this.getView();
        
        grid.getStore().load(function () {
            grid.getSelectionModel().deselectAll();
            grid.disableButtons(true);
        });
    }
});