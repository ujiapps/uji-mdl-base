Ext.define('UJI.grid.Panel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.ujigridpanel',

    requires: [
        'UJI.grid.PanelController',
        'Ext.grid.plugin.RowEditing',
        'Ext.form.Label'
    ],

    controller: 'ujigridpanelcontroller',

    plugins: [{
        ptype: 'rowediting'
    }],

    tbar: [],

    defaultTbar: [{
        xtype: 'button',
        ref: 'add',
        iconCls: 'fa fa-plus',
        text: 'Afegir',
        handler: 'onAdd'
    }, {
        xtype: 'button',
        ref: 'edit',
        iconCls: 'fa fa-edit',
        text: 'Editar',
        disabled: true,
        handler: 'onEdit'
    }, {
        xtype: 'button',
        ref: 'remove',
        iconCls: 'fa fa-remove',
        text: 'Borrar',
        disabled: true,
        handler: 'onDelete'
    }],

    bbar: [{
        xtype: 'button',
        text: 'Recarregar',
        action: 'reload',
        ref: 'reload',
        iconCls: 'fa fa-refresh',
        handler: 'onReload'
    }, {
        xtype: 'label',
        text: '',
        name: 'elementCount',
        ref: 'elementCount'
    }],

    config: {
        allowEdit: true,
        reloadAfterInsert: false,
        showSearchField: false,
        showAddButton: true,
        showEditButton: true,
        showRemoveButton: true,
        showReloadButton: true,
        showTopToolbar: true,
        showBottomToolbar: true
    },

    listeners: {
        render: 'onLoad',
        select: 'onSelect',
        deselect: 'onDeselect',
        cancelEdit: 'onCancelEdit'
    },

    initComponent: function () {
        var ref = this;
        ref.callParent(arguments);
        var tbar = this.getDockedItems('toolbar[dock="top"]')[0];
        var bbar = this.getDockedItems('toolbar[dock="bottom"]')[0];
        tbar.insert(0, ref.defaultTbar);


        if (!this.allowEdit) {
            var rowediting = this.getPlugins().filter(function (plugin) {
                return plugin.ptype === 'rowediting'
            })[0];

            if (rowediting) {
                this.removePlugin(rowediting);
            }
            // this.showAddButton = false;
            // this.showEditButton = false;
            // this.showRemoveButton = false;
        }

        if (this.showSearchField) {
            tbar.insert(0, [
                {
                    xtype: 'searchfield',
                    emptyText: 'Recerca...',
                    store: this.store,
                    width: 180
                }, ' ', '-']);
        }

        if (!this.showAddButton) {
            tbar.down('[ref=add]') && tbar.down('[ref=add]').hide();
        }

        if (!this.showEditButton) {
            tbar.down('[ref=edit]') && tbar.down('[ref=edit]').hide();
        }

        if (!this.showRemoveButton) {
            tbar.down('[ref=remove]') && tbar.down('[ref=remove]').hide();
        }

        if (!this.showReloadButton) {
            bbar.down('[ref=reload]').hide();
        }

        if (!this.showTopToolbar) {
            tbar.hide();
        }

        if (!this.showBottomToolbar) {
            bbar.hide();
        }

        this.store.getProxy().on('exception', function () {
            ref.store.rejectChanges();
        });

        this.store.on('filterchange', function () {
            ref.updateElementCount();
        });

        this.store.on('load', function () {
            ref.updateElementCount();
        });
        this.store.on('write', function () {
            ref.updateElementCount();
        });
    },

    getSelectedId: function () {
        var selection = this.getSelectedRow();

        if (selection) {
            return selection.get("id");
        }
    },

    getSelectedRow: function () {
        var selection = this.getSelection();

        if (selection.length > 0) {
            return selection[0];
        }
    },

    getSelection: function () {
        return this.getSelectionModel().getSelection();
    },

    setUrl: function (id) {
        this.getStore().setUrl(id);
    },
    loadFromUrl: function (id) {
        this.setUrl(id);
        this.getStore().load();
    },

    setParams: function (params) {
        this.getStore().getProxy().extraParams = params;
    },

    reloadData: function () {
        this.getStore().load();
    },

    disableButtons: function (disable) {
        if (!disable) {
            this.down('[ref=edit]') && this.down('[ref=edit]').enable();
            this.down('[ref=edit]') && this.down('[ref=remove]').enable();
        } else {
            this.down('[ref=edit]') && this.down('[ref=edit]').disable();
            this.down('[ref=edit]') && this.down('[ref=remove]').disable();
        }

    },

    clearStore: function () {
        var store = this.getStore();

        store.suspendAutoSync();
        store.removeAll();
        store.clearData();
        store.resumeAutoSync();
    },

    addRecord: function (data) {
        this.store.insert(0, Ext.create(this.store.model, data));
        this.getView().select(0);
    },

    updateElementCount: function () {
        if (!this.getStore())
            return;
        var count = this.getStore().getCount();
        var element = this.down('[name=elementCount]');
        if (element) {
            element.setText(count + ' elements');
        }
    }
});