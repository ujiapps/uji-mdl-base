Ext.define('UJI.form.model.Lookup',
{
    extend : 'Ext.data.Model',
    fields : [ 'id', 'nombre',
    {
        model : 'UJI.form.model.ExtraParam',
        name : 'extraParam'
    } ]
});
