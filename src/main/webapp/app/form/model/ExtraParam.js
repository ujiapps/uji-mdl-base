Ext.define('UJI.form.model.ExtraParam',
{
    extend : 'Ext.data.Model',

    fields : [ 'key', 'value' ]
});
