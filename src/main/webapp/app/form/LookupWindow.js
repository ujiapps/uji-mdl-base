Ext.define('UJI.form.LookupWindow',
{
    extend : 'Ext.Window',

    alias : 'widget.ujilookupWindow',
    appPrefix : '',
    bean : 'base',
    lastQuery : '',
    queryField : '',
    formularioBusqueda : '',
    gridBusqueda : '',
    botonBuscar : '',
    botonCancelar : '',
    extraFields : [],
    title : 'Cercar registres',
    layout : 'border',
    modal : true,
    hidden : true,
    bodyPadding : 10,
    width : 400,
    height : 400,
    closeAction : 'hide',
    clearAfterSearch : true,
    minChars: 3,

    initComponent : function()
    {
        this.callParent(arguments);
        this.initUI();
        this.add(this.formularioBusqueda);
        this.add(this.gridBusqueda);

//        this.addEvents('LookoupWindowClickSeleccion');
    },

    initUI : function()
    {
        this.buildQueryField();
        this.buildBotonBuscar();
        this.buildFormularioBusqueda();
        this.buildBotonCancelar();
        this.buildBotonSeleccionar();
        this.buildGridBusqueda();
    },

    executeSearch : function(query)
    {
        this.gridBusqueda.store.load(
        {
            params :
            {
                query : query,
                bean : this.bean
            }
        });
    },

    buildQueryField : function()
    {
        var ref = this;
        this.queryField = Ext.create('Ext.form.TextField',
        {
            name : 'query',
            value : '',
            listeners :
            {
                specialkey : function(field, e)
                {
                    if (e.getKey() == e.ENTER)
                    {
                        ref.botonBuscar.handler.call(ref.botonBuscar.scope);
                    }
                }
            }
        });
    },

    buildBotonBuscar : function()
    {
        var ref = this;
        this.botonBuscar = Ext.create('Ext.Button',
        {
            text : 'Cerca',
            handler : function(boton, event)
            {
                if (ref.queryField.getValue().length < ref.minChars)
                {
                    Ext.Msg.alert("Error", "Per fer una cerca cal introduir al menys "+ref.minChars+" caracters.");
                }
                else
                {
                    ref.lastQuery = ref.queryField.getValue();
                    ref.executeSearch(ref.queryField.getValue());
                }
            }
        });
    },

    buildFormularioBusqueda : function()
    {
        this.formularioBusqueda = Ext.create('Ext.Panel',
        {
            layout : 'hbox',
            region : 'north',
            height : 45,
            frame : true,
            border: false,
            padding: '5 0 0 0',
            items : [
            {
                xtype : 'label',
                text : 'Expressió: ',
                width : 100,
                padding: '5 0 0 5',
                border: false
            }, this.queryField, this.botonBuscar ]
        });
    },

    buildBotonCancelar : function()
    {
        var ref = this;

        this.botonCancelar = Ext.create('Ext.Button',
        {
            text : 'Cancel.lar',
            handler : function(e)
            {
                ref.hide();
            }
        });
    },

    buildBotonSeleccionar : function()
    {
        var ref = this;

        this.botonSeleccionar = Ext.create('Ext.Button',
        {
            text : 'Seleccionar',
            handler : function(e)
            {
                var record = ref.gridBusqueda.getSelectionModel().getSelection()[0];

                if (record)
                {
                    ref.fireEvent('LookoupWindowClickSeleccion', record);

                    if (ref.clearAfterSearch)
                    {
                        var query = ref.queryField;
                        query.setValue('');
                        ref.gridBusqueda.store.removeAll(true);
                        ref.gridBusqueda.getView().refresh();
                    }
                    ref.hide();
                }
            }
        });
    },

    buildGridBusqueda : function()
    {
        var ref = this;

        var resultColumnList = [
        {
            header : 'Codi',
            width : 50,
            dataIndex : 'id'
        },
        {
            header : 'Nom',
            width : 200,
            dataIndex : 'nombre'
        } ];

        var renderer = function(value, metaData, record, rowIndex, colIndex)
        {
            if (Ext.isDefined(value[(colIndex - 2)].value))
            {
                return value[(colIndex - 2)].value;
            }
        };

        for ( var extraField in this.extraFields)
        {
            if (this.extraFields.hasOwnProperty(extraField))
            {
                resultColumnList.push(
                {
                    header : this.extraFields[extraField],
                    width : 200,
                    dataIndex : 'extraParam',
                    renderer : renderer
                });
            }
        }

        this.gridBusqueda = Ext.create('Ext.grid.Panel',
        {
            region : 'center',
            flex : 1,
            frame : true,
            loadMask : true,
            store : Ext.create('Ext.data.Store',
            {
                model : 'UJI.form.model.Lookup',
                autoSync : false,

                proxy :
                {
                    type : 'ajax',
                    url : '/' + ref.appPrefix + '/rest/lookup',

                    reader :
                    {
                        type : 'json',
                        rootProperty : 'data'
                    }
                },
                listeners :
                {
                    load : function(store, records, successful, eOpts)
                    {
                        if (ref.gridBusqueda.store.data.length === 0)
                        {
                            Ext.Msg.alert("Aviso", "La búsqueda realitzada no ha produït cap resultat");
                        }
                    }
                }
            }),

            columns : resultColumnList,
            forceFit : true,
            listeners :
            {
                celldblclick : function(grid, td, cellindex, record)
                {
                    ref.botonSeleccionar.handler.call(ref.botonSeleccionar.scope);
                }
            },
            buttons : [ this.botonSeleccionar, this.botonCancelar ]

        });
    },

    onEsc : function()
    {
        this.botonCancelar.handler.call(this.botonCancelar.scope);
    },

    listeners :
    {
        'show' : function(window)
        {
            window.queryField.focus(true, 200);
        }
    }
});
