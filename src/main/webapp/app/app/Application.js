Ext.define('UJI.app.Application', {
    extend: 'Ext.app.Application',
    name: 'UJI',
    
    requires: [
        'UJI.grid.PanelController'
    ],

    router: {
        hashbang: true
    },

    init: function() {
        //this.addForeignColumnsToModels();

        var me = this;
        Ext.define('UJI.AppConfig', {
            alias: 'widget.appconfig',
            alternateClassName: 'appConfig',
            singleton: true,
            config:{
                appName: me.name,
                perId:-1,
                cuenta: '',
                nombreCompleto: ''
            }
        });
    },

    addForeignColumnsToModels: function() {
        var me = this;
        Ext.each(this.models, function(modelName) {
            var model = Ext.ModelManager.getModel(me.name + '.model.' + modelName),
                fields = [];
            if (model) {
                Ext.each(model.getFields(), function(field) {
                    if (field.foreign) {
                        fields.push(me.createMappedField(field));
                        field.defaulValue = field.defaulValue || {};
                    }
                    fields.push(field);
                });
                model.setFields(fields);
            }
        });
    },

    createMappedField: function(field) {
        return Ext.create('Ext.data.Field', {
            name: field.name + 'Id',
            type: 'int',
            mapping: field.name + '.id',
            useNull: field.useNull
        });
    }
});