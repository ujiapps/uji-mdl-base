Ext.define('UJI.dashboard.DashboardPanel', {
    extend: 'Ext.panel.Panel',
    alias : 'widget.ujidashboardpanel',

    title: 'Dashboard',
    routeId: 'dashboard',
    bodyPadding : 10,
    html: '<h1>Dashboard</h1>'
});