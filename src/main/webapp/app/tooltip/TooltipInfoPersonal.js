Ext.define('UJI.tooltip.TooltipInfoPersonal', {
    extend: 'Ext.tip.ToolTip',

    alias: 'widget.tooltipinfopersonal',

    config: {
        centerd: true,
        anchor: 'top',
        width: '25%',
        cls: 'tooltipHeader',
        showAnimation: ['fadeIn'],
        showOnTap: "touch",
        autoHide: false,
        closable: false,
        dockposition: {
            top:    false,
            right:  false,
            bottom: true,
            left:   false
        }
    },
    initComponent: function() {
        this.callParent(arguments);
        var source =this.datos.binario?this.datos.binario:Ext.BLANK_IMAGE_URL;
        var nombre =this.datos.nombre?this.datos.nombre:' ';
        var apellido1 =this.datos.apellido1?this.datos.apellido1:' ';
        var cuenta =this.datos.cuenta?this.datos.cuenta+ '@uji.es':' ';


        this.html = '<div class="tip_img"><img src="' + source + '" style="width:55px;height: 73px;" class="imgFoto" ></div>' +
            '<div class="tip_profile"><div><b>' + nombre + '</b> <b>' + apellido1 + '</b></div><div>' + cuenta +'</div></div>' +
            '<div class="tip_bottom"><a href="' + window.location.protocol + '//xmlrpc.uji.es/lsm/logout_sso.php" class="x-btn x-btn-default-toolbar-small" style="color:#264038;">Logout</a></div>';
    }
});