Ext.define('UJI.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    requires: [
        'UJI.tooltip.TooltipInfoPersonal'
    ],
    
    routes: {
        ':node': 'onRouteChange'
    },

    onRouteChange: function (hashTag) {
        var me = this,
            store = me.lookupReference('menuTreeList').getStore();

        if (!store.isLoaded()) {
            store.on('load', function () {
                me.processRoute(hashTag);
                store.un('load', Ext.emptyFn);
            });

            store.load();
        }
        else {
            me.processRoute(hashTag);
        }
    },

    processRoute: function (hashTag) {
        var me = this,
            refs = me.getReferences(),
            mainCard = refs.mainWorkContainerCard,
            mainCardLayout = mainCard.getLayout(),
            menuTreeList = me.lookupReference('menuTreeList'),
            hasTree = menuTreeList && menuTreeList.isVisible();
        var store = menuTreeList.getStore();
        var node = store.findNode('route', hashTag);

        var existingItem = mainCard.child('component[routeId=' + hashTag + ']');

        if (existingItem) {
            mainCardLayout.setActiveItem(existingItem);
        }
        else {
            if (node) {
                Ext.suspendLayouts();
                var id = node.get('id');
                var newPanel = eval("Ext.create('" + id + "')");
                newPanel.routeId = hashTag;
                var activeTab = mainCard.add(newPanel);
                mainCardLayout.setActiveItem(activeTab);
                menuTreeList.setSelection(node);
                Ext.resumeLayouts(true);
            }
        }
        if (hasTree && node) {
            if (node.parentNode && !node.parentNode.isExpanded()) {
                node.parentNode.expand();
            }
        }

    },


    onNavigationTreeSelectionChange: function (tree, node, config) {
        var to = node && (node.get('route'));
        if (to) {
            this.redirectTo(to);
        }
    },

    onImageRender: function (c) {
        var me = this.getView();
        Ext.Ajax.request({
            url: '/' + me.codigoAplicacion.toLowerCase() + '/rest/userinfo',

            success: function (response, opts) {
                var responseJSON = Ext.decode(response.responseText);
                var refs = me.getReferences();

                if (responseJSON.data.binario) {
                    refs.imageLogo.setSrc(responseJSON.data.binario);
                }
                appConfig.setPerId(responseJSON.data.id);
                appConfig.setCuenta(responseJSON.data.cuenta);
                appConfig.setNombreCompleto(responseJSON.data.nombreCompleto);

                Ext.create('UJI.tooltip.TooltipInfoPersonal', {
                    target: c.getEl(),
                    datos: responseJSON.data
                });
            },

            failure: function (response, opts) {
                Ext.create('UJI.tooltip.TooltipInfoPersonal', {
                    target: c.getEl(),
                    datos: ''
                });
            }
        });

    }
});
