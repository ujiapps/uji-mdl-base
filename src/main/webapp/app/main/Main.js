Ext.define('UJI.main.Main', {
    extend: 'Ext.container.Viewport',
    xtype: 'ujiappmain',

    requires: [
        'Ext.list.Tree',
        'Ext.data.TreeStore'
    ],

    controller: 'main',
    viewModel: 'main',

    itemId: 'mainView',

    layout: {
        type: 'border',
        align: 'stretch'
    },

    initComponent: function () {
        var me = this;

        var menuTreeStore = Ext.create('Ext.data.TreeStore', {
            autoLoad: true,

            rootProperty: {
                expanded: true
            },

            proxy: {
                type: 'ajax',
                url: '/' + me.codigoAplicacion.toLowerCase() + '/rest/navigation/class?codigoAplicacion=' + me.codigoAplicacion,
                reader: {
                    type: 'json',
                    rootProperty: 'row'
                }
            }
        });

        var mainWorkContainerCard = {
            xtype: 'container',
            reference: 'mainWorkContainerCard',
            region: 'center',
            flex: 1,
            border: true,
            padding: '10px 10px 10px 0px',
            layout: {
                type: 'card',
                anchor: '100%'
            }
        };

        if (this.dashboard) {
            mainWorkContainerCard.items = [{
                xtype: 'dashboardpanel'
            }];
        }
        ;

        this.items = [{
            xtype: 'panel',
            height: 90,
            region: 'north',
            padding: '8 8 8 8',
            style: 'box-shadow: 0px 0px 8px 1.7px #999; border: 0px; border-bottom: 1px solid #32404e; background-color: white;',
            border: false,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },

            items: [{
                html: '<div class="logo">/div>',
                style: 'background-color: white; border: none; margin-right: 2rem;'
            }, {
                html: '<div style="color: #32404e;"><div style="font-weight:bold; font-size:2rem; margin-top: 0.2rem;">' + this.tituloAplicacion + '</div><div style="font-size:1.2rem; margin-top:0.5rem; margin-bottom: 0.3rem;">' + this.tituloAplicacion + '</div></div>',
                style: 'background-color: white; border: none; margin-top: 0.6rem;',
                flex: 1
            }, {
                xtype: 'image',
                width: 55,
                reference: 'imageLogo',
                alt: 'current user image',
                cls: 'imgFoto',
                listeners: {
                    afterrender: 'onImageRender'
                }
            }]
        }, {
            xtype: 'panel',
            title: me.tituloAplicacion,
            region: 'west',
            split: true,
            //collapsible: true,
            bodyStyle: 'background-color: #32404e',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [{
                xtype: 'treelist',
                reference: 'menuTreeList',
                store: menuTreeStore,
                width: 250,
                ui: 'navigation',
                expanderFirst: false,
                expanderOnly: false,
                listeners: {
                    selectionchange: 'onNavigationTreeSelectionChange'
                }
            }]
        }, mainWorkContainerCard];
        me.callParent(arguments);

    }

});