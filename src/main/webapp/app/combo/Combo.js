Ext.define('UJI.combo.Combo', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.ujicombo',

    valueField: 'id',
    displayField: 'nombre',
    triggerAction: 'all',
    queryMode: 'local',

    showClearIcon: true,
    emptyText: 'Sel·lecciona una opció',
    labelWidth: 70,

    editable: false,
    allowBlank: false,

    triggers: {
        clear: {
            cls: 'x-form-clear-trigger',
            handler: function (cmp) {
                cmp.clearValue();
            },
            scope: 'this',
            weight: -1
        }
    },

    initEvents: function () {
        this.callParent(arguments);
        
        var trigger = this.getTrigger("clear");
        trigger.hide();

        this.addManagedListener(this.bodyEl, 'mouseover', function () {
            if (this.showClearIcon) {
                trigger.show();
            }
        }, this);

        this.addManagedListener(this.bodyEl, 'mouseout', function () {
            trigger.hide();
        });
    },

    initComponent: function () {
        var ref = this;
        ref.callParent(arguments);

        var store = Ext.create('UJI.data.Store', {
            resource: ref.resource,
            subResource: ref.subResource
        });

        ref.store = store;
    },

    loadFromUrl: function (id) {
        this.getStore().setUrl(id);
        this.getStore().load();
    }
});