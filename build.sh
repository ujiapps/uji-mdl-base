#!/bin/bash

export EXTJS_WORKSPACE_HOME=/opt/devel/workspaces/uji/uji-commons-extjs6-workspace
export EXTJS_APP_NAME=MDL
export EXTJS_APP=$EXTJS_WORKSPACE_HOME/apps/mdl
export APP_PATH=$(pwd)
export ENV=production

# Build ExtJS

cd $EXTJS_APP
git fetch && git rebase
sencha app clean
sencha app build -${ENV}

# Copy ExtJS into project

cd $APP_PATH
cp -vR ${EXTJS_WORKSPACE_HOME}/build/${ENV}/${EXTJS_APP_NAME}/* src/main/webapp

# Build Java project

mvn -DskipTests -DskipDocker clean package
